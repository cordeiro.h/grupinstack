<?php
	try{
		require_once("Postagem.php");
		session_start();

		$controle  = new Postagem();
		$blog  = new ModeloPost();

		$blog->setTitulo($_POST['titulo']);
		$blog->setAutor($_POST['autor']);
		$blog->setTexto($_POST['texto']);
		$blog->setResumo($_POST['resumo']);
		$blog->setId($_GET['id']);
		if($controle->editar($blog)){
			$_SESSION["certo"]="true";
			header("Location: ../visual/EditarPost.php?id={$_GET['id']}");
		}else{
			$_SESSION["erro"]="true";
			header("Location: ../visual/EditarPost.php?id={$_GET['id']}");
		}

	}catch(Exception $e){
		echo"Erro geral: {$e->Message()}";
	}
 ?>
