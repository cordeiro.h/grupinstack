<?php
require_once("ControleComentarioBlog.php");
require_once("../modelo/ComentarioBlog.php");
require_once("../modelo/Usuario.php");
session_start();

if(isset($_SESSION["user"])){
	$comentario = new CmtBlog();
	$controle = new ControleCmtBlog();
	$comentario->setCmtBlog($_POST["comentario"]);
	$comentario->setUser($_SESSION['user']->getId());
	$comentario->setPost($_GET['id']);
	if($controle->inserir($comentario)){
	    $_SESSION["erroComentario"]=false;
	    header("Location: ../visual/Visualização.php?id={$_GET['id']}");
	}
}else{
	$_SESSION['cmtBlog'] = $_POST["comentario"];
	header("Location: ../visual/Loguin.php");
}
?>