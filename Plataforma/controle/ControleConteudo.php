<?php
require_once("Conect.php");
require_once("../modelo/Conteudo.php");
class ControleConteudo{
    public function inserir($conteudo){
        $retorno=false;
        try{
            $con=new Conexao();
            $cmd=$con->getConexao()->prepare("INSERT INTO conteudo(arquivo,nome,extensao,curso,video,data) VALUES(:a,:n,:e,:c,:v,:d);");
            $arquivo=$conteudo->getArquivo();
            $nome=$conteudo->getNome();
            $extensao=$conteudo->getExtensao();
            $curso=$conteudo->getCurso();
            $video=$conteudo->getvideo();
            $data=$conteudo->getData();
            $cmd->bindParam("a",$arquivo);
            $cmd->bindParam("n",$nome);
            $cmd->bindParam("e",$extensao);
            $cmd->bindParam("c",$curso);
            $cmd->bindParam("v",$video);
            $cmd->bindParam("d",$data);
            if($cmd->execute()){
                $retorno=true;
            }
            $con->fecharConexao();
            return $retorno;
        }catch(Exception $e){
            echo"Erro ao inserir conteudo: {$e->getMessage()}";
            return $retorno;
        }
    }
    public function selecionarTodos(){
        $retorno=null;
        try{
            $con=new Conexao();
            $cmd=$con->getConexao()->prepare("SELECT * FROM conteudo;");
            if($cmd->execute()){
                $rse=$cmd->fetchAll(PDO::FETCH_CLASS,"Conteudo");
                if($rse!=null){
                    $retorno=$rse;
                }
                $con->fecharConexao();
                return $retorno;
            }
        }catch(Exception $e){
            echo"Erro no selecionarTodos conteudo: {$e->getMessage()}";
            return $retorno;
        }
    }
    public function selecionarUm($conteudo){
        $retorno=null;
        try{
            $con=new Conexao();
            $cmd=$con->getConexao()->prepare("SELECT * FROM conteudo WHERE nome=:n;");
            $nome=$conteudo->getNome();
            $cmd->bindParam(":n",$nome);
            if($cmd->execute()){
                $cmd->setFetchMode(PDO::FETCH_CLASS,"Conteudo");
                $rse=$cmd->fetch();
                if($rse!=null){
                    $retorno=$rse;
                }
            }
            $con->fecharConexao();
            return $retorno;
        }catch(Exception $e){
            echo"Erro no selecionarUm conteudo: {$e->getMessage()}";
            return $retorno;
        }
    }
    public function delete($conteudo){
        $retorno=false;
        try{
            $con = new Conexao();
            $cmd=$con->getConexao()->prepare("DELETE FROM conteudo WHERE id=:i;");
            $id=$conteudo->getId();
            $cmd->bindParam("i",$id);
            if($cmd->execute()){
                $retorno=true;
            }            
            $con->fecharConexao();
            return $retorno;
        }catch(Exception $e){
            echo "Erro no delete conteudo: {$e->getMessage()}";
            return $retorno;
        }

    }
    public function update($conteudo){
        $retorno=false;
        try{
            $con = new Conexao();
            $cmd=$con->getConexao()->prepare("UPDATE conteudo SET nome=:n, arquivo=:a, extensao=:e, curso=:c WHERE id=:i;");
            $id=$conteudo->getId();
            $nome=$conteudo->getNome();
            $arquivo=$conteudo->getArquivo();
            $extensao=$conteudo->getExtensao();
            $curso=$conteudo->getCurso();
            $cmd->bindParam("i",$id);
            $cmd->bindParam("n",$nome);
            $cmd->bindParam("a",$arquivo);
            $cmd->bindParam("e",$extensao);
            $cmd->bindParam("c",$curso);
            if($cmd->execute()){
                $retorno=true;
            }            
            $con->fecharConexao();
            return $retorno;
        }catch(Exception $e){
            echo"Erro no update conteudo:{$e->getMessage()}";
            return $retorno;
        }
    }
    public function selecionarPorCursoC($conteudo){
        $retorno=null;
        try{
            $con=new Conexao();
            $cmd=$con->getConexao()->prepare("SELECT * FROM conteudo WHERE curso=:c AND video=:v;");
            $curso=$conteudo->getCurso();
            $video=$conteudo->getVideo();
            $cmd->bindParam(":c",$curso);
            $cmd->bindParam(":v",$video);
            if($cmd->execute()){
                $rse=$cmd->fetchAll(PDO::FETCH_CLASS,"Conteudo");   
                if($rse!=null){
                    $retorno=$rse;
                }
            }
            $con->fecharConexao();
            return $retorno;
        }catch(Exception $e){
            echo"Erro no selecionarPorCursoC: {$e->getMessage()}";
        }
    }
    public function selecionarUmId($conteudo){
        $retorno=null;
        try{
            $con=new Conexao();
            $cmd=$con->getConexao()->prepare("SELECT * FROM conteudo WHERE id=:i;");
            $id=$conteudo->getId();
            $cmd->bindParam(":i",$id);
            if($cmd->execute()){
                $cmd->setFetchMode(PDO::FETCH_CLASS,"Conteudo");
                $rse=$cmd->fetch();
                if($rse!=null){
                    $retorno=$rse;
                }
            }
            $con->fecharConexao();
            return $retorno;
        }catch(Exception $e){
            echo"Erro no selecionarUmId conteudo: {$e->getMessage()}";
            return $retorno;
        }
    }
}
?>
