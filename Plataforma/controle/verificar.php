<?php
    session_start();
    require_once('ControleUsuario.php');
    require_once('ControleTurma.php');
    require_once('../modelo/Usuario.php');
    try{
        $usuario = new Usuario();
        $usuario->setEmail($_POST['email']);
        $usuario->setSenha($_POST['senha']);
        $turma=new Turma();
        $control = new ControleUsuario();
        if($control->verificar($usuario)){
            $usuario=$control->selecionarUm($usuario);
            if($usuario!=null){
                $_SESSION['user']=$usuario;
                $controle=new ControleTurma();
                $turma->user=$usuario;
                $turma=$controle->selecionarPorUser($turma);
                if($turma!=null){
                    $_SESSION['turma']=$turma;
                    header("Location: ../visual/Principal.php");
                }
            }
        }else{
            echo "<script>alert('Erro na verificação!'); history.back();</script>";
        }
    }catch(Exception $e){
        echo "Erro: $e->getMessage()";
    }
?>
