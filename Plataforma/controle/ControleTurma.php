<?php
require_once("Conect.php");
require_once("../modelo/Turma.php");
class ControleTurma{
    public function inserir($turma){
        $retorno=false;
        try{
            $con=new Conexao();
            $cmd=$con->getConexao()->prepare("INSERT INTO turma(curso,user) VALUES(:c,:u);");
            $curso=$turma->getCurso();
            $user=$turma->user->getId();
            $cmd->bindParam(":c",$curso);
            $cmd->bindParam(":u",$user);
            if($cmd->execute()){
                $retorno=true;
            }
            $con->fecharConexao();
            return $retorno;
        }catch(Exception $e){
            echo"Erro ao inserir turma: {$e->getMessage()}";
            return $retorno;
        }
    }
    public function selecionarPorUser($turma){
        $retorno=null;
        try{
            $con=new conexao();
            $cmd=$con->getConexao()->prepare("SELECT * FROM turma WHERE user=:u;");
            $user=$turma->user->getId();
            $cmd->bindParam(":u",$user);
            if($cmd->execute()){
                $cmd->setFetchMode(PDO::FETCH_CLASS,"Turma");
                $rse=$cmd->fetch();
                if($rse!=null){
                    $retorno=$rse;
                }
            }
            $con->fecharConexao();
            return $retorno;
        }catch(Exception $e){
            echo"Erro no selecionarPorUser turma: {$e->getMessage()}";
            $retorno=null;
        }
    }
    function atualizar($turma){
        $retorno=false;
        try{
            $conexao = new Conexao();
            $curso = $turma->getCurso();
            $user = $turma->user->getId();
            $cmd = $conexao->getConexao()->prepare("UPDATE turma SET curso=:c WHERE user=:u;");
            $cmd->bindParam(":c",$curso);
            $cmd->bindParam(":u",$user);
            if($cmd->execute()){
                $retorno=true;
            }
            $conexao->fecharConexao();
            return $retorno;
        }catch(Exception $e){
            echo "Erro geral (atualizar turma): {$e->getMessage()}";
            return $retorno;
        }
    }

    function deletar($turma){
        $retorno=false;
        try{
            $conexao = new Conexao();                                
            $cmd = $conexao->getConexao()->prepare("DELETE FROM turma WHERE id=:i;");
            $id=$turma->getId();
            $cmd->bindParam(":i",$id);                
            if($cmd->execute()){
                $retorno=true;
            }
            $conexao->fecharConexao();
            return $retorno;
        }catch(PDOException $e){
            echo "Erro no banco (deletar usuario): {$e->getMessage()}";
            return $retorno;
        }catch(Exception $e){
            echo "Erro geral (deletar usuario): {$e->getMessage()}";
            return $retorno;
        }
    }
}
?>
