<?php
    session_start();

    require_once("ControleUsuario.php");
    require_once("ControleTurma.php");

    try{
        $turma=new Turma();
        $turma->user->setNome($_POST['nome']);
        $turma->user->setEmail($_POST['email']);
        $turma->user->setSenha($_POST['senha']);
        $turma->setCurso(0);
        if($_POST['senha']==$_POST['confsenha']){
            $control = new ControleUsuario();
            if($control->cadastrarPessoa($turma->user)){
                $controle=new ControleTurma();
                $turma->user=$control->selecionarUm($turma->user);
                if($controle->inserir($turma)){
                    if($turma->user!=null){
                        $_SESSION["user"]=$turma->user;
                        $turma=$controle->selecionarPorUser($turma);
                        $_SESSION["turma"]=$turma;
                        header("Location: ../visual/Principal.php");
                    }
                }
            }else{
                echo "<script>alert('Usuário já existe!'); history.back();</script>";
            }
        }else{
            header("Location: ../visual/Login.php");
        }
    }catch(Exception $e){
        echo "Erro: $e->getMessage()";
    }
?>
