<?php
	require_once("../modelo/ModeloPost.php");
    require_once('../modelo/ModeloImagem.php');
	require_once("Conect.php");
	class Imagem{
		function inseririmg($imag){
			try{
				$conexao = new Conexao();
				$img = $imag->getNome();
				$tipo = $imag->getTipo();
				$dados = $imag->getDados();
				$post = $imag->getPost();
				$com = $conexao->getConexao()->prepare("INSERT INTO imagem(nome,tipo,dados,post) VALUES(:nome, :tipo, :dados, :post);");
				$com->bindParam("nome", $img);
				$com->bindParam("tipo", $tipo);
				$com->bindParam("dados",$dados);
				$com->bindParam("post",$post);
				if($com->execute()){
					$conexao->fecharConexao();
					return true;
				}else{
					$conexao->fecharConexao();
					return false;
				}
			}catch(PDOException $e){
				echo"Erro PDO: {$e->getMessage()}";
				return false;
			}catch(Exception $e){
				echo"Erro geral: {$e->getMessage()}";
				return false;
			}
		}
		function select($id){
			$conexao = new Conexao();
			$cmd = $conexao->getConexao()->prepare("SELECT * FROM imagem WHERE post=:id");
			$cmd->bindParam("id", $id);
			$cmd->execute();
			$resultado = $cmd->fetch(PDO::FETCH_OBJ);
			return $resultado;
		}
		function select_img_blog(){
		    $conexao = new Conexao();
			//$cmd = $conexao->getConexao()->prepare("SELECT i.dados, i.id from imagem as i inner join post as p on p.id =   i.post where p.id=:id;");
			$cmd = $conexao->getConexao()->prepare("SELECT * from imagem;");
			//$cmd->bindParam("id", $post);
			$cmd->execute();
			$resultado = $cmd->fetchAll(PDO::FETCH_OBJ);
			return $resultado;
		}
		function upimg($imagg){
			try{
				$conexao = new Conexao();

				$img = $imagg->getNome();
				$tipo = $imagg->getTipo();
				$dados = $imagg->getDados();
				$id = $imagg->getId();
				$cmd = $conexao->getConexao()->prepare("UPDATE imagem SET nome=:nome, tipo=:tipo, dados=:dados WHERE id=:id");
				$cmd->bindParam("id", $id);
				$cmd->bindParam("nome", $img);
				$cmd->bindParam("tipo", $tipo);
				$cmd->bindParam("dados",$dados);
				if($cmd->execute()){
					$conexao->fecharConexao();
					return true;
				}else{
					$conexao->fecharConexao();
					return false;
				}

			}catch(PDOException $e){
				echo"Erro PDO: {$e->getMessage()} ";
			}
		}

	}

 ?>
