<?php
    require_once("Conect.php");
    //require_once("../Modelo/Usuario.php");
    class ControleUsuario{

        //Seleciona toda tabela

        function selecionarTodos(){
            try{
                $con = new Conexao();
                $cmd = $con->getConexao()->prepare("SELECT * FROM usuario;");
                $cmd->execute();
                $resultado = $cmd->fetchAll(PDO::FETCH_ASSOC);
                return $resultado;
            }catch(PDOException $e){
                echo "Erro no banco (selecionarTodos): {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral (selecionarTodos): {$e->getMessage()}";
            }
        }

        //Seleciona uma pessoa pelo email
        function selecionarUm($user){
            $retorno=null;
            try{
                $con=new Conexao();
                $cmd=$con->getConexao()->prepare("SELECT * FROM usuario WHERE email=:e;");
                $email=$user->getEmail();
                $cmd->bindParam("e",$email);
                if($cmd->execute()){
                    $cmd->setFetchMode(PDO::FETCH_CLASS,"Usuario");
                    $rse=$cmd->fetch();
                    if($rse!=null){
                        $retorno=$rse;
                    }
                }
                $con->fecharConexao();
                return $retorno;
            }catch(Exception $e){
                echo"Erro no selecionarUm: {$e->getMessage()}";
                return $retorno;
            }
        }

        //Seleciona uma pessoa específica

        function selecionarPid($id){
            try{
                $conexao = new Conexao();
                $cmd = $conexao->getConexao()->prepare("SELECT * FROM usuario WHERE id=:id;");
                $cmd->bindParam("id", $id);
                $cmd->execute();
                $cmd->setFetchMode(PDO::FETCH_CLASS,"Usuario");
                $resultado=$cmd->fetch();
                $conexao->fecharConexao();
                return $resultado;
            }catch(PDOException $e){
                echo "Erro no banco (selecionarPid): {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral (selecionarPid): {$e->getMessage()}";
            }
        }

        //Atualiza a tabela

        function atualizar($usuario){
            $retorno=false;
            try{
                $conexao = new Conexao();
                $n = $usuario->getNome();
                $senha = $usuario->getSenha();
                $id = $usuario->getId();

                $cmd = $conexao->getConexao()->prepare("UPDATE usuario SET nome=:n,senha=:s WHERE id=:i;");
                $cmd->bindParam(":i",$id);
                $cmd->bindParam(":n",$n);
                $cmd->bindParam(":s",$senha);
                if($cmd->execute()){
                    $retorno=true;
                }
                $conexao->fecharConexao();
                return $retorno;
            }catch(PDOException $e){
                echo "Erro no banco (atualizar): {$e->getMessage()}";
                return $retorno;
            }catch(Exception $e){
                echo "Erro geral (atualizar): {$e->getMessage()}";
                return $retorno;
            }
        }


        //Verifica se existe na tabela

        function verificar($usuario){
            try{
                $conexao = new Conexao();
                $e = $usuario->getEmail();
                $s =  md5($usuario->getSenha());
                $cmd = $conexao->getConexao()->prepare("SELECT * FROM usuario WHERE email = :e && senha = :s;");
                $cmd->bindParam(":e",$e);
                $cmd->bindParam(":s",$s);
                $cmd->execute();
                if($cmd->rowCount() == 1){
                    $conexao->fecharConexao();
                    return true;
                }else{
                    $conexao->fecharConexao();
                    return false;
                }
            }catch(PDOException $e){
                echo "ERRO do (verificar do) banco:{$e->getMessage()}";
            }catch(Exception $e){
                echo "ERRO (verificar) geral:{$e->getMessage()}";
            }
        }

        //Cadastra pessoas

        function cadastrarPessoa($usuario){
            try{
                $conexao = new Conexao();
                $e = $usuario->getEmail();
                $n = $usuario->getNome();
                $s = md5($usuario->getSenha());
                $cmd = $conexao->getConexao()->prepare("SELECT * FROM  usuario WHERE email = :e;");
                $cmd->bindParam(":e",$e);
                $cmd->execute();
                if($cmd->rowCount() >0 ){
                    $conexao->fecharConexao();
                    return false;
                }else{
                    $cmd = $conexao->getConexao()->prepare("INSERT INTO usuario (nome, email, senha, adm) VALUES(:n,:e,:s, 0);");
                    $cmd->bindParam("n",$n);
                    $cmd->bindParam("e",$e);
                    $cmd->bindParam("s",$s);                    
                    if($cmd->execute()){
                            $conexao->fecharConexao();
                            return true;
                    }else{
                            $conexao->fecharConexao();
                            return false;
                    }
                }
            }catch(PDOException $e){
                echo "ERRO do (cadastrarPessoa do) banco:{$e->getMessage()}";
            }catch(Exception $e){
                echo "ERRO (cadastrarPessoa) geral:{$e->getMessage()}";
            }
        }

        function deletar($usuario){
            $retorno=false;
            try{
                $conexao = new Conexao();                                
                $cmd = $conexao->getConexao()->prepare("DELETE FROM usuario WHERE id=:i;");
                $id=$usuario->getId();
                $cmd->bindParam(":i",$id);                
                if($cmd->execute()){
                    $retorno=true;
                }
                $conexao->fecharConexao();
                return $retorno;
            }catch(PDOException $e){
                echo "Erro no banco (deletar usuario): {$e->getMessage()}";
                return $retorno;
            }catch(Exception $e){
                echo "Erro geral (deletar usuario): {$e->getMessage()}";
                return $retorno;
            }
        }

        //Login off

        function sair(){
            try{
                session_destroy();
                header("Location: ../Visual/login.php");
            }catch(PDOException $e){
                echo "ERRO do (sair do) banco:{$e->getMessage()}";
            }catch(Exception $e){
                echo "ERRO (sair) geral:{$e->getMessage()}";
            }
        }

    }
?>
