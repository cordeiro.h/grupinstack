<?php
require_once("ImgControle.php");
require_once("../modelo/ImgPerfil.php");
require_once("../modelo/Usuario.php");
session_start();

$controle = new ImgControle();
$imag     = new ImgPerfil();

$imagem=$_FILES['img'];
$imag->setNome($imagem['name']);
$imag->setExtensao($imagem['type']);
$imag->setArquivo(file_get_contents($imagem['tmp_name']));
$imag->setUser($_SESSION['user']->getId());

$test = $controle->selecionarUm($_SESSION['user']->getId());


if($test==False){
	if($controle->inserir($imag)){
		$_SESSION['insert'] = "uma foto";
		//echo "<script>alert('In');</script>";
		header("Location: ../visual/Conf.php");
	}else{
		echo "<script>alert('Erro ao inserir. Preencha corretamente todos os campos!!');window.location.replace('../visual/Conf.php')</script>";
	}
}else{
	if($controle->upimg($imag)){
		$_SESSION['insert'] = "uma foto";
		header("Location: ../visual/Conf.php");
		//echo "<script>alert('UP');</script>";
		echo $_SESSION['user']->getId();
	}else{
		echo "<script>alert('Erro ao atualizar. Preencha corretamente todos os campos!!');window.location.replace('../visual/Conf.php')</script>";
	}
}

?>