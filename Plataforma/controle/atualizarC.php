<?php
require_once("ControleTurma.php");
require_once("ControleUsuario.php");
session_start();
$turma=new Turma();
$controleT=new ControleTurma();
$controleU=new ControleUsuario();
$turma->user->setId($_SESSION["user"]->getId());
if($_POST['senha']!=""){
    $turma->user->setNome($_POST["nome"]);
    $turma->user->setSenha(md5($_POST["senha"]));
    if($controleU->atualizar($turma->user)){
        $_SESSION["erroUpdate"]=false;
    }else{
        $_SESSION["erroUpdate"]=true;
    }
    if($_SESSION["turma"]->getCurso()!=0){
        $turma->setCurso($_POST["curso"]);
        if($controleT->atualizar($turma)){
            $_SESSION["user"]=$controleU->selecionarUm($_SESSION["user"]);
            $_SESSION["turma"]->user->setId($_SESSION["user"]->getId());
            $_SESSION["turma"]=$controleT->selecionarPorUser($_SESSION["turma"]);
            header("Location: ../visual/Conf.php");
        }else{
            $_SESSION["erroUpdateT"]=true;
            header("Location: ../visual/Conf.php");
        }
    }else{
        $_SESSION["user"]=$controleU->selecionarUm($_SESSION["user"]);
        header("Location: ../visual/Conf.php");
    }
}else{
    $turma->user->setNome($_POST["nome"]);
    $turma->user->setSenha($_SESSION["user"]->getSenha());
    if($controleU->atualizar($turma->user)){
        $_SESSION["erroUpdate"]=false;
    }else{
        $_SESSION["erroUpdate"]=true;
    }
    if($_SESSION["turma"]->getCurso()!=0){
        $turma->setCurso($_POST["curso"]);
        if($controleT->atualizar($turma)){
            $_SESSION["user"]=$controleU->selecionarUm($_SESSION["user"]);
            $_SESSION["turma"]->user->setId($_SESSION["user"]->getId());
            $_SESSION["turma"]=$controleT->selecionarPorUser($_SESSION["turma"]);
            header("Location: ../visual/Conf.php");
        }else{
            $_SESSION["erroUpdateT"]=true;
            header("Location: ../visual/Conf.php");
        }
    }else{
        $_SESSION["user"]=$controleU->selecionarUm($_SESSION["user"]);
        header("Location: ../visual/Conf.php");
    }
}
?>