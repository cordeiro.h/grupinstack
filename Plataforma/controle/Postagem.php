<?php
    require_once("../modelo/ModeloPost.php");
    require_once("Conect.php");
    class Postagem{
        function inserir($post){
            try{
                $conexao = new Conexao();

                $titulo = $post->getTitulo();
                $autor = $post->getAutor();
                $texto = $post->getTexto();
                $data = $post->getData();
                $resumo = $post->getResumo();
                $cmd =  $conexao->getConexao()->prepare("INSERT INTO post(titulo,autor,texto,data,resumo) VALUES(:titulo,:autor,:texto,:data,:resumo);");
                $cmd->bindParam("titulo",$titulo);
                $cmd->bindParam("autor",$autor);
                $cmd->bindParam("texto", $texto);
                $cmd->bindParam("data", $data);
                $cmd->bindParam("resumo", $resumo);
                if($cmd->execute()){
                    $ultimo_id = $conexao->getConexao()->lastInsertId();
                    $conexao->fecharConexao();
                    return $ultimo_id;
                }else{
                    $conexao->fecharConexao();
                    return false;
                }
            }catch(PDOException $e){
                echo"Erro PDO: {$e->getMessage()}";
                return false;

            }catch(Exception $e){
                echo"Erro no Banco: {$e->getMessage()}";
                return false;
            }
        }

        function selecionar($id){
            try{
                $conexao = new Conexao();
                $cmd = $conexao->getConexao()->prepare("SELECT * from post where id=:id;");
                $cmd->bindParam(':id',$id);
                if($cmd->execute()){
                    $resultado=$cmd->fetchAll(PDO::FETCH_CLASS,"ModeloPost");
                    $conexao->fecharConexao();
                    return $resultado;
                }else{
                    $conexao->fecharConexao();
                    return false;
                }
            }catch(PDOException $e){
                echo "Erro PDO: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }

        }

        function selecionarTodos(){
            try{
                $conexao = new Conexao();
                $sql= $conexao->getConexao()->prepare("SELECT * FROM post");
                $sql->execute();
                $result = $sql->fetchAll(PDO:: FETCH_CLASS, "ModeloPost");
                return $result;
            }catch(PDOException $e){
                echo "Erro PDO: {$e->getMessage()}";

            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }

        function editar($postt){
            try{
                $conexao = new Conexao();
                $blog = new ModeloPost();
                $id = $postt->getId();
                $titulo = $postt->getTitulo();
                $autor = $postt->getAutor();
                $texto = $postt->getTexto();
                $resumo = $postt->getResumo();
                $cmd = $conexao->getConexao()->prepare("UPDATE post SET titulo=:titulo, texto=:texto, autor=:autor, resumo=:resumo WHERE id=:id");
                $cmd->bindParam("titulo", $titulo);
                $cmd->bindParam("autor",$autor);
                $cmd->bindParam("texto", $texto);
                $cmd->bindParam("resumo", $resumo);
                $cmd->bindParam("id", $id);
                if($cmd->execute()){
                        $conexao->fecharConexao();
                        return true;
                    }else{
                        $conexao->fecharConexao();
                        return false;
                    }
            }catch(PDOException $e){
                echo "Erro PDO: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }
        function delPost($post){
            $retorno=false;
            try{
                $conexao = new Conexao();
                $cmd = $conexao->getConexao()->prepare("DELETE FROM imagem WHERE post=:i;");
                $cmd2 = $conexao->getConexao()->prepare("DELETE FROM post WHERE id=:i;");
                $cmd->bindParam(":i",$post);
                $cmd2->bindParam(":i",$post);
                if($cmd->execute()){
                  if($cmd2->execute()){
                    $retorno=true;
                  }
                }
                $conexao->fecharConexao();
                return $retorno;
            }catch(PDOException $e){
                echo "Erro no banco (deletar usuario): {$e->getMessage()}";
                return $retorno;
            }catch(Exception $e){
                echo "Erro geral (deletar usuario): {$e->getMessage()}";
                return $retorno;
            }
        }
    }
 ?>
