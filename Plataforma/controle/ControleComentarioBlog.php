<?php
require_once("Conect.php");
require_once("../modelo/ComentarioBlog.php");
class ControleCmtBlog{
    public function inserir($comentario){
        $retorno=false;
        try{
            $con=new Conexao();
            $cmd=$con->getConexao()->prepare("INSERT INTO cmtBlog(cmtBlog,user,post) VALUES(:c, :u, :p);");
            $coment=$comentario->getCmtBlog();
            $user=$comentario->getUser();
            $post=$comentario->getPost();
            $cmd->bindParam(":c",$coment);
            $cmd->bindParam(":u",$user);
            $cmd->bindParam(":p",$post);
            if($cmd->execute()){
                $retorno=true;
            }
            $con->fecharConexao();
            return $retorno;
        }catch(Exception $e){
            echo"Erro ao inserir comentario: {$e->getMessage()}";
            return $retorno;
        }
    }
    public function selecionarPorPost($post){
        $retorno=null;
        try{
            $con=new Conexao();
            $cmd=$con->getConexao()->prepare("SELECT * FROM cmtBlog WHERE post=:p;");
            $cmd->bindParam(":p",$post);
            if($cmd->execute()){
                $rse=$cmd->fetchAll(PDO::FETCH_CLASS,"CmtBlog");
                if($rse!=null){
                    $retorno=$rse;
                }
            }
            $con->fecharConexao();
            return $retorno;
        }catch(Exception $e){
            echo"Erro ao inserir comentario: {$e->getMessage()}";
            return $retorno;
        }
    }
    public function delete($comentario){
        $retorno=false;
        try{
            $con=new Conexao();
            $cmd=$con->getConexao()->prepare("DELETE FROM cmtBlog WHERE id=:i;");            
            $id=$comentario->getId();
            $cmd->bindParam("i",$id);                        
            if($cmd->execute()){
                $retorno=true;
            }
            $con->fecharConexao();
            return $retorno;
        }catch(Exception $e){
            echo"Erro ao deletar comentario: {$e->getMessage()}";
            return $retorno;
        }
    }
}
?>
