<?php
require_once("ControleUsuario.php");
require_once("../modelo/Usuario.php");
require_once("../modelo/Turma.php");
session_start();

try{
    $usuario = new Usuario();
    $turma = new Turma();

    $n = $_SESSION['user']->getNome();

    $usuario->setNome($n);
    $usuario->setSenha($_POST['senhaAtual']);
    $control = new ControleUsuario();

    if (md5($_POST['senhaAtual'])==$_SESSION['user']->getSenha()) {
        $usuario->setId($_SESSION['user']->getId());
        $usuario->setSenha($_POST['novaSenha']);
        $usuario->setNome($_POST['nome']);
        $usuario->setEmail($_SESSION['user']->getEmail());
        $turma->setCurso($_POST['curso']);

        if($control->atualizar($usuario, $turma)){
            $_SESSION["atualizar"]='funcionou';
            header("Location: ../visual/Conf.php");
        }else{
            echo "<script>alert('Erro ao atualizar!'); history.back();</script>";
        }
    }else{
      echo "<script>alert('Senha atual incorreta!'); history.back();</script>";
    }

}catch(Exception $e){
    echo "Erro: $e->getMessage()";
}

?>
