<?php
require_once("Conect.php");
require_once("../modelo/ImgPerfil.php");
class ImgControle{
    public function inserir($img){
        $retorno=false;
        try{
            $con=new Conexao();
            $cmd=$con->getConexao()->prepare("INSERT INTO imgPerfil(arquivo,nome,extensao,user) VALUES(:a,:n,:e,:u);");                
            $arquivo=$img->getArquivo();
            $nome=$img->getNome();
            $extensao=$img->getExtensao();
            $user=$img->getUser();
            $cmd->bindParam("a",$arquivo);
            $cmd->bindParam("n",$nome);
            $cmd->bindParam("e",$extensao);
            $cmd->bindParam("u",$user);
            if($cmd->execute()){
                $retorno = true;
            }
            $con->fecharConexao();
            return $retorno;
        }catch(Exception $e){
            echo"Erro ao inserir imagem: {$e->getMessage()}";
            return $retorno;
        }
    }

    public function upimg($img){
        $retorno=false;
        try{
            $con=new Conexao();
            $cmd=$con->getConexao()->prepare("UPDATE imgPerfil SET arquivo=:a, nome=:n, extensao=:e WHERE user=:u;");                
            $arquivo=$img->getArquivo();
            $nome=$img->getNome();
            $extensao=$img->getExtensao();
            $user=$img->getUser();
            $cmd->bindParam("a",$arquivo);
            $cmd->bindParam("n",$nome);
            $cmd->bindParam("e",$extensao);
            $cmd->bindParam("u",$user);
            if($cmd->execute()){
                $retorno = true;
            }
            $con->fecharConexao();
            return $retorno;
        }catch(Exception $e){
            echo"Erro ao atualizar a imagem: {$e->getMessage()}";
            return $retorno;
        }
    }

    //public function selecionarUm($img){

    function selecionarUm($id){
            $conexao = new Conexao();
            $cmd = $conexao->getConexao()->prepare("SELECT * FROM imgPerfil WHERE user=:id");
            $cmd->bindParam("id", $id);
            $cmd->execute();
            $resultado = $cmd->fetch(PDO::FETCH_OBJ);
            return $resultado;
        }
}
?>
