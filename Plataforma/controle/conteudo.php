<?php
session_start();
require_once("ControleConteudo.php");
$conteudo=new Conteudo();
$controle=new ControleConteudo();
$conteudo->setCurso($_POST["curso"]);
$conteudo->setNome($_FILES["arquivo"]["name"]);
$conteudo->setExtensao($_FILES["arquivo"]["type"]);
$conteudo->setVideo(0);
$conteudo->setArquivo(file_get_contents($_FILES["arquivo"]["tmp_name"]));
$conteudo->setData(strval(date("d / m / y")));
if($controle->inserir($conteudo)){
    $_SESSION["erroConteudo"]=false;
}else{
    $_SESSION["erroConteudo"]=true;
}
header("Location: ../visual/ConteudosA.php");
?>
