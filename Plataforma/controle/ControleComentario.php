<?php
require_once("Conect.php");
require_once("../modelo/Comentario.php");
class ControleComentario{
    public function inserir($comentario){
        $retorno=false;
        try{
            $con=new Conexao();
            $cmd=$con->getConexao()->prepare("INSERT INTO comentario(comentario,user,video) VALUES(:c, :u, :v);");
            $coment=$comentario->getComentario();
            $user=$comentario->user->getId();
            $video=$comentario->video->getId();
            $cmd->bindParam("c",$coment);
            $cmd->bindParam("u",$user);
            $cmd->bindParam("v",$video);
            if($cmd->execute()){
                $retorno=true;
            }
            $con->fecharConexao();
            return $retorno;
        }catch(Exception $e){
            echo"Erro ao inserir comentario: {$e->getMessage()}";
            return $retorno;
        }
    }
    public function selecionarPorVideo($comentario){
        $retorno=null;
        try{
            $con=new Conexao();
            $cmd=$con->getConexao()->prepare("SELECT * FROM comentario WHERE video=:v;");
            $video=$comentario->video->getId();
            $cmd->bindParam("v",$video);
            if($cmd->execute()){
                $rse=$cmd->fetchAll(PDO::FETCH_ASSOC);
                if($rse!=null){
                    $retorno=$rse;
                }
            }
            $con->fecharConexao();
            return $retorno;
        }catch(Exception $e){
            echo"Erro ao inserir comentario: {$e->getMessage()}";
            return $retorno;
        }
    }
    public function update($comentario){
        $retorno=false;
        try{
            $con=new Conexao();
            $cmd=$con->getConexao()->prepare("UPDATE comentario SET comentario=c,user=u,video=v WHERE id=:i;");
            $coment=$comentario->getComentario();
            $user=$comentario->user->getId();
            $video=$comentario->video->getId();
            $cmd->bindParam("c",$coment);
            $cmd->bindParam("u",$user);
            $cmd->bindParam("v",$video);
            if($cmd->execute()){
                $retorno=true;
            }
            $con->fecharConexao();
            return $retorno;
        }catch(Exception $e){
            echo"Erro ao atualizar comentario: {$e->getMessage()}";
            return $retorno;
        }
    }
    public function delete($comentario){
        $retorno=false;
        try{
            $con=new Conexao();
            $cmd=$con->getConexao()->prepare("DELETE FROM comentario WHERE id=:i;");            
            $id=$comentario->getId();
            $cmd->bindParam("i",$id);                        
            if($cmd->execute()){
                $retorno=true;
            }
            $con->fecharConexao();
            return $retorno;
        }catch(Exception $e){
            echo"Erro ao deletar comentario: {$e->getMessage()}";
            return $retorno;
        }
    }
    public function deletePorIdUser($userId){
        $retorno=false;
        try{
            $con=new Conexao();
            $cmd=$con->getConexao()->prepare("DELETE FROM comentario WHERE user=:i;");            
            $cmd->bindParam("i",$userId);                        
            if($cmd->execute()){
                $retorno=true;
            }
            $con->fecharConexao();
            return $retorno;
        }catch(Exception $e){
            echo"Erro ao deletar comentario: {$e->getMessage()}";
            return $retorno;
        }
    }
}
?>
