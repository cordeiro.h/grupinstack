<?php
if(isset($_GET["id"])){
    require_once("ImgControle.php");
    $img=new ImgPerfil();
    $control=new ImgControle();
    $img->user->setId($_GET["id"]);
    $img=$control->selecionarUm($img);
    if($img!=null){
        header("Content-Type: {$img['extensao']}");
        echo $img["arquivo"];
    }
}else{
    require_once("ControleConteudo.php");
    $conteudo=new Conteudo();
    $controle=new ControleConteudo();
    $conteudo->setNome($_GET["nome"]);
    $conteudo=$controle->selecionarUm($conteudo);
    if($conteudo!=null){
        header("Content-Type: {$conteudo->getExtensao()}");
        echo "{$conteudo->getArquivo()}";
    }
}
?>
