<?php
require_once("ControleUsuario.php");
require_once("ControleTurma.php");
require_once("ControleComentario.php");
session_start();
$controleU = new ControleUsuario();
$controleT = new ControleTurma();
$controleC = new ControleComentario();
if($controleT->deletar($_SESSION["turma"])){
    if($controleC->deletePorIdUser($_SESSION["user"]->getId())){
        if($controleU->deletar($_SESSION["user"])){
            $_SESSION["erroDel"]=false;
            header("Location: ../");
        }else{
            $_SESSION["erroDel"]=true;
            header("Location: ../visual/Conf.php");
        }
    }else{
        $_SESSION["erroDel"]=true;
        header("Location: ../visual/Conf.php");    
    }
}else{
    $_SESSION["erroDel"]=true;
    header("Location: ../visual/Conf.php");
}
?>