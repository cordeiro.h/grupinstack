<?php
require_once("../modelo/Usuario.php");
require_once("../modelo/Turma.php");
require_once("../controle/Imagem.php");
require_once("../controle/Postagem.php");
session_start();
echo"
<!DOCTYPE html>
<html>
<head>
  <title>Blog | World Dev</title>
  <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>    
    <link rel='stylesheet' href='css/bootstrap.css'>
	<link rel='stylesheet' href='css/bloggg.css'>
	<link rel='icon' href='imagens/logo.png'>
</head>
<body style='background: #1a3966'>
  	<div class='container-fluid d-flex flex-column px-0'>
		<div class='menu-topo d-lg-flex justify-content-lg-between align-items-lg-center'>
			<!--Menu-->
			<nav class='navbar'>
				<div class='menu-toggle' id='mobile-menu'>
					<span class='bar'></span>
					<span class='bar'></span>
					<span class='bar'></span>
				</div>
				<ul class='nav-menu' id='men'>
					<li><a href='../index.php' class='nav-links '>Home</a></li>
					<li><a href='#sobre' class='nav-links'>Sobre</a></li>
					<li><a href='#c' class='nav-links'>Criadores</a></li>
					<li><a href='Loguin.php' class='nav-links'>Login</a></li>
          ";
          if(isset($_SESSION['user'])){
            if($_SESSION["user"]->getAdm()==1){
              echo "<li><a href='VisualizaçãoA.php' class='nav-links'>Post's</a></li>";
            }
          }
          echo "
				</ul>
			</nav>
		</div>
    	<div class='container' style='background-color: #fff'>
    		<center>
      			  <div>
						<h4 class='topo mt-5' id='tex'>
							<span class='vai'>Blog World Dev</span>
							<span class='cur'>|</span>
						</h4>
    					<br>
    					<p class='topo'>Um mundo de possibilidades!</p>
    					<br>
				  </div>
			</center>
			<hr class='featurette-divider' style='margin-top: -8px; color: #81acea; z-index: -1 !important'>
			<center>
				<br><br>
				<p class='top'>Leia nossas publicações sobre o mundo da tecnologia e divirta-se!</p>
  			</center>
        ";
$postagem = new Postagem();
$conteudo=$postagem->selecionarTodos();

$cnt = count($conteudo);
if($cnt!=0){
  echo"<div class='mt-5'>
    			<div class='container'>
      				<div class='row'>";
              $y = $cnt;
              for ($i = 0; $i < $cnt; $i++) {
                  $y = $y-1;
                  echo "
        				<div class='col-lg-4 col-md-6 mb-5'>
          				<div class='card shadow-sm'>
           						<img class='bd-placeholder-img card-img-top' src='../controle/imgBlog.php?id={$conteudo[$y]->getId()}' width='100%' height='250px'></img>
            					<div class='card-body'>
              						<p class='card-text'>{$conteudo[$y]->getTitulo()}</p>
              						<div class='d-flex justify-content-between align-items-center'>
                						<div class='btn'>
                  							<a type='button' href='Visualização.php?id={$conteudo[$y]->getId()}' class='btn btn-sm btn-outline-secondary'>Ver Post</a>
                						</div>
                						<small class='text-muted p-3'>{$conteudo[$y]->getData()}</small>
              				     </div>
                      </div>
            			</div>
              </div>

                  ";
                }
              }
              echo "
              </div>
          		</div>
        	</div>
  		</div>
  		<br>

      	<div class='container marketing'>
      	<hr class='featurette-divider'>
      	<br>
      	<div class='row featurette'>
          <div class='col-md-7' style='margin: auto 0 auto 0'>
            <h2 class='featurette-heading' id='sobre'>Sobre o Blog World Dev</h2>
			<p class='lead' id='p'>Com o Blog World Dev você pode ver posts diferenciados sobre à tecnologia. 
			Assim iremos te mostrar ideias novas, pensamentos novos, ou até a história da informática! Você VAI AMAR esse mundo! </p>
          </div>
          <div class='col-md-5'>
            <img class='featurette-image img-fluid mx-auto' src='imagens/blog.jpg'  alt='Generic placeholder image'>
          </div>
        </div>
		<br>
		</div>
        	<hr class='featurette-divider' style='color: #81acea'>
		  <br>
		<div class='container mt-5'>
      	<div class='row featurette'>
          <div class='col-md-5 mt-3'>
          	<img class='featurette-image img-fluid mx-auto' src='imagens/logo.png'  alt='Generic placeholder image' style='margin-top: -2em;'>
          </div>
          <div class='col-md-7' style='margin: auto 0 auto 0'>          
          	<h2 class='featurette-heading' id='c'>Criação</h2>
            <p class='lead' id='u'>O Blog World Dev foi criado pelos administradores da platarfoma, afim de transmitir mais conhecimentos sobre a área de informática em meio a tempos tão difíceis como a pandemia de Covid-19</p>
          </div>
		</div>
		<a href='#' style='font-size: 20px; text-decoration: none; opacity: 0.9; color: #c0d5f4' class='mb-5'><img id='seta' src='imagens/seta.png'></a>
        </div>
        <br>                	        
        <br>
  	</div>
</body>
<script src='js/jquery-3.4.1.min.js'></script>
<script type='text/javascript'>
	const menu = document.querySelector('#mobile-menu');
	const menuLinks = document.querySelector('.nav-menu');

	menu.addEventListener('click', function() {
		menu.classList.toggle('is-active');
		menuLinks.classList.toggle('active')
	})
</script>
<script src='js/b.js'></script>
</html>
";

?>
