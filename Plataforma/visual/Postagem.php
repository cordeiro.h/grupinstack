<?php
session_start();

  $data = date('d-m-Y');
echo"
<!DOCTYPE html>
<html>
<head>
  <title>Plataforma | Projeto Social</title>
  <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
    <link rel='stylesheet' href='css/bootstrap.min.css'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
    <link rel='stylesheet' href='css/bootstrap.css'>
    <link rel='stylesheet' href='css/visual.css'>
</head>
<body>
  <div class='container marketing'>
    <hr class='featurette-divider'>
    <center>
      <h2 class='post'>Criar nova postagem</h2>
    </center>
    <hr class='featurette-divider'>
    <div class='row featurette'>
      <div class='col-md-5' order-md-1>
        <div >
           <img src='imagens/a.jpg' class='featurette-image img-fluid mx-auto' alt='Generic placeholder image'>
        </div>
      </div>
      <div class='col-md-7'>
        <div class='posi'>
          <center>
            <h4 class='font-italic'>Postagem</h4>
            <br>
            <div class='col-md-7'>
              <form class='form vertical-alignC' action='../controle/post.php'  method='post' enctype='multipart/form-data'>
                <input type='hidden' name='data' value='{$data}' />
                <div class='form-group'>
                  <input type='text' id='nome' class='form-control' name='titulo' placeholder='Titulo ' required>
                  <br>
                  <input type='text' name='resumo' class='form-control' placeholder='Resumo' required>
                  <br>
                  <input type='text' name='autor' class='form-control' placeholder='Autor' required>
                  <br>
                  <textarea rows='2' name='texto'  placeholder='Texto' required></textarea><br><br>
                  <label for='img'><a style='color: blue;'>Selecionar imagem</a></label>
                  <input type='file' id='img' name='img' required>
                  <br><br>
                  <center>
                    <input type='submit' value='Enviar' class='btn btn-outline-secondary' tabindex='-1'></input>
                  </center>
                  <br>
                </div>
              </form>
            </div>
          </center>
        </div>
      </div>
    </div>
    <center>
      <a href='VisualizaçãoA.php'>Voltar<a>
    </center>
  </div>
</body>
<script src='js/bootstrap.min.js' integrity='sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy' crossorigin='anonymous'></script>
<script src='js/sweetAlert.js'></script>
</html>
";
if(isset($_SESSION["post"])){
    echo"<script>swal('Enviado','Um novo post foi criado','success');</script>";
    unset($_SESSION["post"]);
}

?>
