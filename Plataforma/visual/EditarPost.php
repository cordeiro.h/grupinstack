<?php
session_start();
require_once("../controle/Postagem.php");
$post = new Postagem();
$infPost = $post->selecionar($_GET['id']);

$data = date('Y-m-d');
echo"
<!DOCTYPE html>
<html>
<head>
  <title>Plataforma | Projeto Social</title>
  <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
    <link rel='stylesheet' href='css/bootstrap.min.css'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
    <link rel='stylesheet' href='css/bootstrap.css'>
    <link rel='stylesheet' href='css/visual.css'>
</head>
<body>
  <div class='container marketing'>
    <hr class='featurette-divider'>
    <center>
      <h2 class='post'>Editar postagem</h2>
    </center>
    <hr class='featurette-divider'>
    <div class='row featurette'>
      <div class='col-md-5' order-md-1>
        <div >
           <img src='../controle/imgBlog.php?id={$_GET['id']}' class='featurette-image img-fluid mx-auto' alt='Generic placeholder image'>
        </div>
      </div>
      <div class='col-md-7'>
        <div class='posi'>
          <center>
            <h4 class='font-italic'>Postagem</h4>
            <br>
            <div class='col-md-7'>
              <form class='form vertical-alignC' action='../controle/editarPost.php?id={$_GET['id']}'  method='post'>
              <input type='hidden' name='data' value='{$_GET['id']}' />
              <input type='hidden' name='id' value='' />
                <div class='form-group'>
                  <input type='text' id='nome' value='{$infPost['0']->getTitulo()}' class='form-control' name='titulo' placeholder='Titulo ' required>
                  <br>
                  <input type='text' value='{$infPost['0']->getAutor()}' class='form-control' name='autor' placeholder='Autor ' required>
                  <br>
                  <input type='text' name='resumo' value='{$infPost['0']->getResumo()}' class='form-control' aria-describedby='emailHelp' placeholder='Resumo' required>
                  <br>
                  <textarea rows='2' name='texto'  placeholder='Texto' required>{$infPost['0']->getTexto()}</textarea><br><br>
                  <center>
                  <label for='img'><a style='color: blue;'>Selecionar imagem</a></label>
                  <input type='file' id='img' name='img' required>
                  <br><br>
                    <input type='submit' value='Editar' class='btn btn-outline-secondary' tabindex='-1' />
                  </center>
                  <br>
                </div>
              </form>
            </div>
          </center>
        </div>
      </div>
    </div>
    <center>
      <a href='VisualizaçãoA.php'>Voltar<a>
    </center>
  </div>
</body>
<script>
<
<script src='js/bootstrap.min.js' integrity='sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy' crossorigin='anonymous'></script>
<script src='js/jquery.js'></script>
<script src='js/sweetAlert.js'></script>

</html>
";
if(isset($_SESSION["certo"])){
    echo"<script>swal('Atenção','Edição feita, confira se não errou mais nada!','warning');</script>";
    unset($_SESSION["certo"]);
}
if(isset($_SESSION["erro"])){
    echo"<script>swal('Atenção','Criatura, tu errou alguma coisa','warning');</script>";
    unset($_SESSION["erro"]);
}

?>
