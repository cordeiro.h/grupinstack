<?php
session_start();
echo "
<!DOCTYPE html>
<html lang='pt-br'>
<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' href='css/geral.css'>
    <link rel='icon' href='imagens/duvi.webp'>
    <title>Ajuda | World Dev</title>
</head>
<body>
    <header class='mb-5 d-flex flex-column flex-md-row align-items-center p-lg-4 p-md-3 p-sm-3 px-md-4 bg-white border-bottom shadow-sm' style='position: fixed; width: 100%'>
        <p class='h5 my-sm-3 my-lg-0 me-md-auto fw-normal'>World Dev</p>
        <nav class='my-2 my-md-0 me-md-3'>
            <a class='p-2 text-dark' href='Principal.php'>Menu</a>
            <a class='p-2 text-dark' href='Conf.php'>Configurações</a>
            <a class='p-2 text-dark' href='Ajuda.php'>Ajuda</a>
            <a class='p-2 text-dark' href='../controle/sair.php'>Sair</a>
        </nav>
    </header>

    <div class='container'>
    <br><br><br><br><br>
        <div class='row'>
            <div class='col-lg-6'>
                <img src='imagens/ajuda.jpg' class='img-fluid'>
            </div>
            <div class='col-lg-4'>
                <p style='font-size: 40px; color:#3b3b3b'>Dúvidas?</p>
                <p style='font-size: 18px; color: #636363;'>Permaneça aqui!</p>
                <p style='text-align: justify; color: #313131'>Caso tenha alguma dúvida e tenha vergonha de perguntar em público
                você pode ficar a vontade aqui e mandar diretamente para o nosso email de forma privada. Além disso, se tiver dúvidas
                sobre como usar nossa plataforma, atente para as informações a seguir
                </p>
            </div>
        </div>
</div>
        <div style='background-color: #81acea;'>
        <div class='container' class='p-5'>
        <div class='card mt-5 mb-5' style='border-style: solid #f09'>
            <p class='text-center' style='font-size: 30px; margin-top: 4%'>Envie-nos um email</p>
            <hr>
            <div style='padding: 40px'>
            <form action='../PHPMailer/envio.php' method='post'>
                <div class='row'>
                    <div class='col-lg-4 offset-lg-1 p-lg-3'>
                        <div class='form-group'>
                            <label for='nome'>Nome:</label>
                            <input class='form-control' id='nome' name='nome' type='nome' placeholder='Nome ou apelido' required/>
                        </div>
                        <br>
                        <div class='form-group'>
                            <label for='text'>Dúvida ou Comentário:</label>
                            <textarea class='form-control' id='text' name='comentario' rows='3' placeholder='Tire sua dúvida ou nos dê um feedback' required></textarea>
                        </div>
                        <br>
                        <input class='btn btn-dark' type='submit' value='Enviar'>
                    </div>
                    <div class='col-lg-7'>
                        <img class='img-fluid' src='imagens/mande.gif'>
                    </div>
                </div>
            </form>
            </div>
        </div>
        </div>
    </div>

</body>
<script src='js/sweetAlert.js'></script>
</html>
";
if(isset($_SESSION["mail"])){
    echo"<script>swal('Enviado','Sua dúvida foi enviada','success');</script>";
    unset($_SESSION["mail"]);
}
if(isset($_SESSION["smail"])){
    echo"<script>swal('Erro','Seu e-mail não pôde ser enviado','error');</script>";
    unset($_SESSION["smail"]);
}
?>
