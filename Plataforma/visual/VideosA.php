<?php
require_once("../controle/ControleConteudo.php");
require_once("../modelo/Turma.php");
session_start();
$controle=new ControleConteudo();
$conteudo=new Conteudo();
$conteudo->setVideo(1);
$conteudo->setCurso(1);
$conteudo2=new Conteudo();
$conteudo2->setCurso(2);
$conteudo2->setVideo(1);
echo "
    <!DOCTYPE html>
    <html lang='pt-br'>
    <head>
        <meta name='viewport' content='width=device-width, initial-scale=1'>    
        <link rel='stylesheet' type='text/css' href='css/gerall.css'>
        <title>Videos Adm | World Dev</title>
        <link rel='icon' href='imagens/vide.png'>
    </head>
    <body style='background-color:#3981e6'>
        <header class='d-flex flex-column flex-md-row align-items-center p-lg-4 p-md-3 p-sm-3 px-md-4 bg-white border-bottom shadow-sm' id='menu'>
            <p class='h5 my-lg-0 me-md-auto fw-normal mt-4'><img src='imagens/logoV.png' width='120'></p>
            <nav class='my-2 my-md-0 me-md-3'>            
                <a class='p-2 text-dark' href='Principal.php'>Inicio</a>                                                
                <a class='p-2 text-dark' href='Conf.php'>Configurações</a>
                <a class='p-2 text-dark' href='Ajuda.php'>Ajuda</a> 
                <a class='p-2 text-dark' href='../controle/sair.php'>Sair</a>                        
            </nav>                    
        </header>
        <br> </br>       
        <div class='container'>
            <div class='my-3 p-3 bg-white rounded shadow-sm mt-5'>
                <div class='row'>
                    <div class='col-lg-6'>
                        <img src='imagens/videosA.jpg' class='img-fluid'>
                    </div>
                    <div class='col-lg-4'>                
                        <p style='font-size: 40px; color:black' class='text-center'>Enviar Vídeos</p>
";
if(!isset($_GET["id"])){
    echo"
                        <form class='caiF p-lg-5 p-md-5' style='position: relative !important' action='../controle/conteudoV.php' method='post' enctype='multipart/form-data'>
                            <div class='row'>
                                <div class='form-group mb-2'>
                                    <label class='mb-1' for='nome'>Vídeo a postar:</label>
                                    <select class='form-control' name='curso' id=''>                                        
                                        <option value='1'>LP</option>
                                        <option value='2'>HTML/CSS</option>
                                    </select>
                                </div>                                                     
                                <div class='form-group'>
                                    <label id='nameA'> </label>
                                    <label class='mt-2 mb-1 btn bg-primary' id='inp' for='anexar'>Selecionar arquivo</label>                                    
                                    <input type='file' name='arquivo' id='anexar'/>                                    
                                </div>
                            </div>
                            <br>
                            <input style='color: #fff;' class='btn bg-primary mt-3' type='submit' value='Enviar' />
                        </form>                                                
    ";
}else{
    echo"
                        <form class='caiF p-lg-5 p-md-5' style='position: relative !important' action='../controle/atualizarV.php' method='post' enctype='multipart/form-data'>
                            <input type='hidden' name='id' value='{$_GET["id"]}' />
                            <div class='row'>
                                <div class='form-group mb-2'>
                                    <label class='mb-1' for='nome'>Vídeo a postar:</label>
                                    <select class='form-control' name='curso' id=''>                                        
                                        <option value='1'>LP</option>
                                        <option value='2'>HTML/CSS</option>
                                    </select>
                                </div>                                                     
                                <div class='form-group'>
                                    <label id='nameA'> </label>
                                    <label class='mt-2 mb-1 btn bg-primary' id='inp' for='anexar'>Selecionar arquivo</label>                                    
                                    <input type='file' name='arquivo' id='anexar'/>                                    
                                </div>
                            </div>
                            <br>
                            <input style='color: #fff;' class='btn bg-primary mt-3' type='submit' value='Enviar' />
                        </form>                                                
    ";
}
echo"
                    </div>                    
                </div>
";
if(!isset($_GET["id"])){
    $conteudo=$controle->selecionarPorCursoC($conteudo);
    if($conteudo!=null){
    echo"
                <div class='mt-5'>
                                <p style='font-size: 20px; color:#384b66' class='text-center'>Vídeos de LP</p>
                                <div class='table-responsive mb-5'>
                                    <table class='table'>
                                        <thead>
                                            <th scope='col'>Vídeo</th>
                                            <th scope='col'>Data</th>
                                            <th scope='col'>Atualizar</th>
                                            <th scope='col'>Deletar</th>
                                        </thead>
                                        <tbody>
    ";
    foreach($conteudo as $item){
    echo"
                                            <tr>
                                                <td><a href='../controle/download.php?nome={$item->getNome()}' target='_blank'>{$item->getNome()}</a></td>
                                                <td>{$item->getData()}</td>
                                                <td><a href='VideosA.php?id={$item->getId()}'><img src='imagens/atu.png'></a></td>
                                                <td><a href='../controle/deleteV.php?id={$item->getId()}'><img src='imagens/del.png'></a></td>
                                            </tr>
    ";
    }
    echo"                                    
                                        <tbody>
                                    </table>
                                </div>                                            
    ";
    }
    $conteudo2=$controle->selecionarPorCursoC($conteudo2);
    if($conteudo2!=null){
    echo"
                                <p style='font-size: 20px; color:#384b66' class='text-center'>Vídeos de Html/Css</p>
                                <div class='table-responsive mb-5'>
                                    <table class='table'>
                                        <thead>
                                            <th scope='col'>Vídeo</th>
                                            <th scope='col'>Data</th>
                                            <th scope='col'>Atualizar</th>
                                            <th scope='col'>Deletar</th>
                                        </thead>
                                        <tbody>
    ";
    foreach($conteudo2 as $item){
    echo"
                                            <tr>
                                                <td><a href='../controle/download.php?nome={$item->getNome()}' target='_blank'>{$item->getNome()}</a></td>
                                                <td>{$item->getData()}</td>
                                                <td><a href='VideosA.php?id={$item->getId()}'><img src='imagens/atu.png'></a></td>
                                                <td><a href='../controle/deleteV.php?id={$item->getId()}'><img src='imagens/del.png'></a></td>
                                            </tr>
    ";
    }
    echo"  
                                        <tbody>
                                    </table>
                                </div>
                    
    ";
    }
}
if(isset($_SESSION['erroDelete'])){
    if($_SESSION['erroDelete']){
        echo "<script>swal('Aff','Deu erro','error');</script>";
    }else{
        echo "<script>swal('Top','Deletou','success');</script>";
    }
    unset($_SESSION['erroDelete']);
}
if(isset($_SESSION['erroUpdate'])){
    if($_SESSION['erroUpdate']){
        echo "<script>swal('Ops','Deu erro','error');</script>";
    }else{
        echo "<script>swal('Ok','Atualizou','success');</script>";
    }
    unset($_SESSION['erroUpdate']);
}

echo"
            </div>
        </div>
        <script src='js/file.js'></script>
    </body>    
    </html>
";
?>
