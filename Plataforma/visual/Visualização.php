<?php
require_once("../controle/Postagem.php");
require_once("../modelo/Usuario.php");
require_once("../controle/ControleUsuario.php");
session_start();
$post = new Postagem();
$infPost = $post->selecionar($_GET['id']);

require_once("../controle/ControleComentarioBlog.php");
$cB = new ControleCmtBlog();
$cmtBlog = $cB->selecionarPorPost($_GET['id']);

$carct = nl2br($infPost['0']->getTexto());

echo "
<!DOCTYPE html>
<html lang='pt-br'>
<head>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' href='css/visu.css'>
    <title>Visualização | World Dev</title>
    <link rel='icon' href='imagens/logo.png'>
</head>
<body>
<header class='d-flex flex-column flex-md-row align-items-center p-lg-4 p-md-3 p-sm-3 px-md-4 bg-white border-bottom shadow-sm' id='menu'>
<p class='h5 my-sm-3 my-lg-0 me-md-auto fw-normal'>World Dev</p>
<nav class='my-2 my-md-0 me-md-3'>
    <a class='p-2 text-dark' href='Blog.php'>Inicio</a>
    <a class='p-2 text-dark' href='../controle/sair.php'>Sair</a>
</nav>
</header>
<br> <br>
    <div class='container'>
    <div class='t'>
    <h1  style='text-align: center;'class='titulo'>{$infPost['0']->getTitulo()}</h1>

    </div>
    <div class='row mb-5'>    
        <div class='col-lg-6 offset-lg-3'>
            <img src='../controle/imgBlog.php?id={$_GET['id']}' class='img-fluid' alt='Imagem responsiva'>
        </div>
        <div class='col-lg-8 offset-lg-2'>
            <div class='texto' style='text-align: justify'>
                {$carct}
            </div>
        </div>
   </div>
    <hr>
    <div class='autor text-center'>
    <br>
    <div class='curtir'>{$infPost['0']->getAutor()}</a><span aria-hidden='true'> · </span>{$infPost['0']->getData()}</a><span aria-hidden='true'>
    </div>
    </div>
            <div class='row mt-5'>
                <form action='../controle/InsertCmtBlog.php?id={$_GET['id']}' method='post'>
                    <h2 class='mt-5 text-center'>
                        Comentários
                    </h2>
                    <div class='row mt-4'>
                        <div class='comentario'>
                            <div class='col-lg-1'>
                                <img class='img-thumbnail rounded-circle img-comentario' src='https://image.flaticon.com/icons/png/512/59/59170.png'>
                            </div>
                            <div class='col-lg-10 textarea'>";
                        if(isset($_SESSION['cmtBlog'])){
                            echo "
                                <textarea class='form-control' id='text' name='comentario' rows='2' placeholder='Adicione um Comentário' required>{$_SESSION['cmtBlog']}</textarea>";
                                unset($_SESSION['cmtBlog']);
                        }else{
                            echo "
                                <textarea class='form-control' id='text' name='comentario' rows='2' placeholder='Adicione um Comentário' required></textarea>";
                        }
                            echo "
                            </div>
                            <div class='col-lg-1 ml-2'>
                                <button class='btn btn-primary btn-enviar'>
                                    Enviar
                                </button>
                            </div>
                        </div>
                    </div>";

            $control = new ControleUsuario();
                        
            if(!is_null($cmtBlog)){
                $cnt = count($cmtBlog);
                $y = $cnt;
                for ($i = 0; $i < $cnt; $i++) {
                  $y = $y-1;

                  $users = $control->selecionarPid($cmtBlog[$y]->getUser());

                    echo "
                    <div class='row mt-5'>
                        <div class='comentario'>
                            <div class='col-lg-1'>
                                <img class='img-thumbnail rounded-circle img-comentario' src='https://image.flaticon.com/icons/png/512/59/59170.png'>
                            </div>
                            <div class='col-lg-11'>
                               <b >{$users->getNome()}</b>
                               <p>{$cmtBlog[$y]->getCmtBlog()}</p>
                               ";
                        if(isset($_SESSION['user'])){
                            if($_SESSION['user']->getId()==$users->getId()){
                                $_SESSION['xBlg'] = $_GET['id'];
                               echo "
                               <div class='curtir'><a href='../controle/delComentBlog.php?id={$cmtBlog[$y]->getId()}'>Excluir</a><span aria-hidden='true'>";
                            }
                        }
                               echo "
                            </div>
                        </div>
                    </div>
                    ";
                }
            }
                    echo "
                </form>
            </div>
        </div>

</div>

</html>
";
?>
