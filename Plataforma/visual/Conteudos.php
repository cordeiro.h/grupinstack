<?php
require_once("../modelo/Turma.php");
session_start();
require_once("../controle/ControleConteudo.php");
$controle=new ControleConteudo();
$conteudo=new Conteudo();
$conteudo->setVideo(0);
if($_SESSION['turma']->getCurso()==3 || $_SESSION['turma']->getCurso()==0){
    $conteudo2=new Conteudo();
    $conteudo->setCurso(1);
    $conteudo2->setCurso(2);
    $conteudo2->setVideo(0);
}else{
    $conteudo->setCurso($_SESSION['turma']->getCurso());
}
echo "
<!DOCTYPE html>
<html lang='pt-br'>
<head>
    <meta name='viewport' content='width=device-width, initial-scale=1'>    
    <link rel='stylesheet' type='text/css' href='css/gerall.css'>
    <link rel='icon' href='imagens/ativ.png'>
    <title>Conteúdos | World Dev</title>
</head>
<body style='background-color: #81acea'>
    <header class='d-flex flex-column flex-md-row align-items-center p-lg-4 p-md-3 p-sm-3 px-md-4 bg-white border-bottom shadow-sm' style='position: fixed; width: 100%; margin-bottom: 10%' id='menu'>
        <p class='h5 my-sm-3 my-lg-0 me-md-auto fw-normal'>World Dev</p>        
        <nav class='my-2 my-md-0 me-md-3'>            
            <a class='p-2 text-dark' href='Principal.php'>Inicio</a>
            <a class='p-2 text-dark' href='Ajuda.php'>Ajuda</a>
            <a class='p-2 text-dark' href='../controle/sair.php'>Sair</a>
        </nav>                    
    </header>
    <br><br><br><br>
    <div class='container'>
        <div class='my-3 p-3 bg-white rounded shadow-sm'>
            <div class='row'>
                <div class='col'>
                    <img src='imagens/conteudo.jpg' class='img-fluid'>
                </div>
                <div class='col-lg-4' style='margin: auto 0 auto 0'>
                    <p style='font-size: 40px; color:#384b66'>Conteúdos</p>
                    <p style='font-size: 20px; color:#384b66'>Fique atento para não perder nenhuma atividade.</p>
                    <p style='text-align: justify'>Ola dev, aqui você terá acesso aos exercícios postados e slides das aulas, caso tenha dúvidas
                    você poderá ir até a página de <a href='Ajuda.php' target='_blank' style='text-decoration: none'>ajuda</a> e tirar sua dúvida diretamente
                    pelo email da equipe. 
                    </p>
                </div>
            </div>
";
if(isset($conteudo2)){
    $conteudo=$controle->selecionarPorCursoC($conteudo);
    $conteudo2=$controle->selecionarPorCursoC($conteudo2);
    if($conteudo!=null){
        if($_SESSION['turma']->getCurso()==1){
        echo"
            <hr class='mb-3'>
                <p style='font-size: 40px; color:#384b66' class='text-center'>Exerícios e Slides Lógica de Programação</p>
            <hr class='mb-5'>
            <div class='table-responsive mb-5'>
                <table class='table'>
                    <thead>
                        <th scope='col'>Curso</th>
                        <th scope='col'>Data</th>
                        <th scope='col'>Exercicio/Slide</th>
                    </thead>            
                    <tbody>
";
        foreach($conteudo as $item){
echo"
                        <tr>
                            <th scope='row'>Lógica de Programação</th> 
                            <td>{$item->getData()}</td>
                            <td><a href='../controle/download.php?nome={$item->getNome()}'>{$item->getNome()}</a></td>
                        </tr>                  
";
        }
echo"
                    </tbody>
                </table>
            </div>
";
        }else{
            echo"
                <hr class='mb-3'>
                    <p style='font-size: 40px; color:#384b66' class='text-center'>Exerícios e Slides Lógica de HTML/CSS</p>
                <hr class='mb-5'>
                <div class='table-responsive mb-5'>
                    <table class='table'>
                        <thead>
                            <th scope='col'>Curso</th>
                            <th scope='col'>Data</th>
                            <th scope='col'>Exercicio/Slide</th>
                        </thead>            
                        <tbody>
";
        foreach($conteudo as $item){
echo"
                        <tr>
                            <th scope='row'>HTML/CSS</th> 
                            <td>{$item->getData()}</td>
                            <td><a href='../controle/download.php?nome={$item->getNome()}'>{$item->getNome()}</a></td>
                        </tr>                  
";
        }
echo"
                    </tbody>
                </table>
            </div>
";
        }
    }
    if($conteudo2!=null){
echo"
        <hr class='mb-3'>
            <p style='font-size: 40px; color:#384b66' class='text-center'>Exerícios e Slides HTML/CSS</p>
        <hr class='mb-5'>
        <div class='table-responsive mb-5'>
            <table class='table'>
                <thead>
                    <th scope='col'>Curso</th>
                    <th scope='col'>Data</th>
                    <th scope='col'>Exercicio/Slide</th>
                </thead>            
                <tbody>
";
        foreach($conteudo2 as $item){
echo"
                    <tr>
                        <th scope='row'>HTML/CSS</th> 
                        <td>{$item->getData()}</td>
                        <td><a href='../controle/download.php?nome={$item->getNome()}'>{$item->getNome()}</a></td>
                    </tr>                  
";  
        }
echo"
                </tbody>
            </table>
        </div>
";
    }
    
}else{
    $conteudo=$controle->selecionarPorCursoC($conteudo);
    if($conteudo!=null){

        echo"
            <hr class='mb-3'>
                <p style='font-size: 40px; color:#384b66' class='text-center'>Exerícios e Slides Lógica de Programação</p>
            <hr class='mb-5'>
            <div class='table-responsive mb-5'>
                <table class='table'>
                    <thead>
                        <th scope='col'>Curso</th>
                        <th scope='col'>Data</th>
                        <th scope='col'>Exercicio/Slide</th>
                    </thead>            
                    <tbody>
";
        foreach($conteudo as $item){
echo"
                        <tr>
                            <th scope='row'>Lógica de Programação</th> 
                            <td>{$item->getData()}</td>
                            <td><a href='../controle/download.php?nome={$item->getNome()}'>{$item->getNome()}</a></td>
                        </tr>                  
";
        }
echo"
                    </tbody>
                </table>
            </div>
";
    }
}
echo"
        </div>
    </div>
</body>
</html>
";
