<?php
require_once("../controle/Imagem.php");
require_once("../controle/Postagem.php");
session_start();

echo "
<!DOCTYPE html>
<html lang='pt-br'>
<head>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' href='css/visuA.css'>
    <title>Visualização | World Dev</title>
    <link rel='icon' href='imagens/ativ.png'>
</head>
<body style='background-color:#4169E1;'>
    
    <header class='d-flex flex-column flex-md-row align-items-center p-lg-4 p-md-3 p-sm-3 px-md-4 bg-white border-bottom shadow-sm' id='menu'>
        <p class='h5 my-lg-0 me-md-auto fw-normal mt-4'><img src='imagens/logoV.png' width='120'></p>
        <nav class='my-2 my-md-0 me-md-3'>
            <a class='p-2 text-dark' href='../index.php'>Home</a>
            <a class='p-2 text-dark' href='Blog.php'>Blog</a>
            <a class='p-2 text-dark' href='../controle/sair.php'>Sair</a>
        </nav>
    </header>

        <div class='container'>
            <div class='my-3 p-3 bg-white rounded shadow-sm mt-5'>
                <div class='row'>
                    <div class='col-lg-6'>
                        <img src='imagens/la.jpeg' class='img-fluid'>
                    </div>
                    <div class='col-lg-4'>
                        <p style='font-size: 40px; color:#384b66' class='text-center'>Criar</p>
                        <form class='caiF p-lg-5 p-md-5' style='position: relative !important' action='Postagem.php' method='post'>
                            <div class='row'>
                                <div class='form-group mb-2 'style='text-align:center;'>
                                    <label class='mb-1 text-center' for=''>Postagem</label>
                                </div>
                            <br>
                            <input style='color: #fff;' class='btn bg-primary mt-3' type='submit' value='Criar' />
                        </form>
                    </div>
                </div>

                <div class='mt-5'>
                            <p style='font-size: 20px; color:#384b66' class='text-center'>Postagens</p>
                            <div class='table-responsive mb-5'>
                                <table class='table'>
                                    <thead>
                                        <th scope='col'>Nome</th>
                                        <th scope='col'>Id</th>
                                        <th scope='col'>Atualizar</th>
                                        <th scope='col'>Deletar</th>
                                    </thead>
                                    <tbody>";
$postagem = new Postagem();
$conteudo=$postagem->selecionarTodos();

$cnt = count($conteudo);

if($cnt!=0){
echo"<div class='mt-5'>";
for ($i = 0; $i < $cnt; $i++) {
                                          echo"
                                                <tr>
                                                    <td><a target='blank'>{$conteudo[$i]->getTitulo()}</a></td>
                                                    <td>{$conteudo[$i]->getId()}</td>
                                                    <td><a href='../controle/editarPost.php?id={$conteudo[$i]->getId()}'><img src='imagens/atu.png'></a></td>
                                                    <td><a href='../controle/delPost.php?id={$conteudo[$i]->getId()}'><img src='imagens/del.png'></a></td>
                                                </tr>
                                              ";
                                            }
                                    }else{
                                        echo"
                                            <h1 class='text-center'>Não Existem arquivos no banco</h1>
                                        ";
                                    }

                                    echo "
                                    <tbody>
                                </table>

        </div>
        <script src='js/file.js'></script>
        </nav>

</body>
<script src='js/sweetAlert.js'></script>
</html>
";
if(isset($_SESSION["delPost"])){
    echo"<script>swal('Atenção','Apagou!','warning');</script>";
    unset($_SESSION["delPost"]);
}

?>
