<?php
if($_SESSION["videoInicial"]!=""){
    //Usar videoInicial para insert e select
    $comentario=new Comentario();
    $controleComentario=new ControleComentario();
    $comentario->video->setNome($_SESSION["videoInicial"]);
    $comentario->video=$controle->selecionarUm($comentario->video);
    echo "
        <div class='container'>
            <div class='row mt-5'>
                        <form action='../controle/insertComent.php' method='post'>
                            <input type='hidden' name='user' value='{$_SESSION["user"]->getId()}' />
                            <input type='hidden' name='video' value='{$comentario->video->getId()}' />
                            <h2 class='mt-5 text-center'>
                                Comentários
                            </h2>
                            <div class='row mt-4'>
                                <div class='comentario'>
                                    <div class='col-lg-1'>
                                        <img class='img-thumbnail rounded-circle img-comentario' src='https://image.flaticon.com/icons/png/512/59/59170.png'>  
                                    </div>
                                    <div class='col-lg-10 textarea'>
                                        <textarea class='form-control' id='text' name='comentario' rows='2' placeholder='Adicione um Comentário' required></textarea>
                                    </div>
                                    <div class='col-lg-1 ml-2'>
                                        <button class='btn btn-primary btn-enviar'>
                                            Enviar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
    ";
    $comentarios=$controleComentario->selecionarPorVideo($comentario);
    if($comentarios!=null){
        foreach($comentarios as $item){
            $nomeUsuario=$controleUsuario->selecionarPid($item["user"]);
            if($item["user"]!=$_SESSION["user"]->getId()){
                echo"           <div class='row mt-5'>
                                    <div class='comentario'>
                                        <div class='col-lg-1'>
                                            <img class='img-thumbnail rounded-circle img-comentario' src='https://image.flaticon.com/icons/png/512/59/59170.png'>  
                                        </div>
                                        <div class='col-lg-11'>
                                            <h5>{$nomeUsuario['nome']}</h5>
                                            <p>{$item['comentario']}</p>
                                        </div>
                                    </div>
                                </div>
                ";
            }else{
                echo"           <div class='row mt-5'>
                                    <div class='comentario'>
                                        <div class='col-lg-1'>
                                            <img class='img-thumbnail rounded-circle img-comentario' src='https://image.flaticon.com/icons/png/512/59/59170.png'>  
                                        </div>
                                        <div class='col-lg-11'>
                                            <h5>{$nomeUsuario['nome']}</h5>
                                            <p>{$item['comentario']}</p>
                                            <a href='../controle/deleteComent.php?id{$item["id"]}'>Apagar comentário</a>
                                        </div>
                                    </div>
                                </div>
                ";
            }
        }
    }
    echo"
                </div>
            </div>
        </div>
    ";
}else{
    //Fazer select dos videos e pegar o primeiro resultado
    $comentario=new Comentario();
    $controleComentario=new ControleComentario();
    if($_SESSION["turma"]->getCurso()==3){
        //Selecionar todos e pegar o primeiro resultado
        $comentario->video=$controle->selecionarTodos();
        if($comentario->video!=null){
            foreach($comentario->video as $item){
                if($item->getVideo()==1){
                    $comentario->video=$item;
                    $key=0;
                    break;
                }
            }
            if(isset($key)){
                echo "
                <div class='container'>
                    <div class='row mt-5'>
                                <form action='../controle/insertComent.php' method='post'>
                                    <input type='hidden' name='user' value='{$_SESSION["user"]->getId()}' />
                                    <input type='hidden' name='video' value='{$comentario->video->getId()}' />
                                    <h2 class='mt-5 text-center'>
                                        Comentários
                                    </h2>
                                    <div class='row mt-4'>
                                        <div class='comentario'>
                                            <div class='col-lg-1'>
                                                <img class='img-thumbnail rounded-circle img-comentario' src='https://image.flaticon.com/icons/png/512/59/59170.png'>  
                                            </div>
                                            <div class='col-lg-10 textarea'>
                                                <textarea class='form-control' id='text' name='comentario' rows='2' placeholder='Adicione um Comentário' required></textarea>
                                            </div>
                                            <div class='col-lg-1 ml-2'>
                                                <button class='btn btn-primary btn-enviar'>
                                                    Enviar
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                ";
                $comentarios=$controleComentario->selecionarPorVideo($comentario);
                if($comentarios!=null){
                    foreach($comentarios as $item){
                        //Selecionar o nome do usuario de acordo com o id
                        $nomeUsuario=$controleUsuario->selecionarPid($item["user"]);
                        if($item["user"]!=$_SESSION["user"]->getId()){
                            echo"
                                            <div class='row mt-5'>
                                                <div class='comentario'>
                                                    <div class='col-lg-1'>
                                                        <img class='img-thumbnail rounded-circle img-comentario' src='https://image.flaticon.com/icons/png/512/59/59170.png'>  
                                                    </div>
                                                    <div class='col-lg-11'>
                                                        <h5>{$nomeUsuario["nome"]}</h5>                                        
                                                        <p>{$item["comentario"]}</p>
                                                    </div>
                                                </div>
                                            </div>
                            ";
                        }else{
                            echo"
                                            <div class='row mt-5'>
                                                <div class='comentario'>
                                                    <div class='col-lg-1'>
                                                        <img class='img-thumbnail rounded-circle img-comentario' src='https://image.flaticon.com/icons/png/512/59/59170.png'>  
                                                    </div>
                                                    <div class='col-lg-11'>
                                                        <h5>{$nomeUsuario["nome"]}</h5>                                        
                                                        <p>{$item["comentario"]}</p>
                                                        <a href='../controle/deleteComent.php?id={$item["id"]}'>Apagar comentário</a>
                                                    </div>
                                                </div>
                                            </div>
                            ";
                        }
                    }
                }
                echo"
                            </div>
                        </div>
                    </div>
                ";
            }
            unset($key);
        }
    }else{
        //Selecionar por curso e pegar o primeiro resultado
        $comentario->video->setCurso($_SESSION["turma"]->getCurso());
        $comentario->video->setVideo(1);
        $comentario->video=$controle->selecionarPorCursoC($comentario->video);
        if($comentario->video!=null){
            foreach($comentario->video as $item){
                $comentario->video=$item;
                $key=0;
                break;
            }
            if(isset($key)){
                echo "
                <div class='container'>
                    <div class='row mt-5'>
                                <form action='../controle/insertComent.php' method='post'>
                                    <input type='hidden' name='user' value='{$_SESSION["user"]->getId()}' />
                                    <input type='hidden' name='video' value='{$comentario->video->getId()}' />
                                    <h2 class='mt-5 text-center'>
                                        Comentários
                                    </h2>
                                    <div class='row mt-4'>
                                        <div class='comentario'>
                                            <div class='col-lg-1'>
                                                <img class='img-thumbnail rounded-circle img-comentario' src='https://image.flaticon.com/icons/png/512/59/59170.png'>  
                                            </div>
                                            <div class='col-lg-10 textarea'>
                                                <textarea class='form-control' id='text' name='comentario' rows='2' placeholder='Adicione um Comentário' required></textarea>
                                            </div>
                                            <div class='col-lg-1 ml-2'>
                                                <button class='btn btn-primary btn-enviar'>
                                                    Enviar
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                ";
                $comentarios=$controleComentario->selecionarPorVideo($comentario);
                if($comentarios!=null){
                    foreach($comentarios as $item){
                        //Selecionar o nome do usuario de acordo com o id
                        $nomeUsuario=$controleUsuario->selecionarPid($item["user"]);
                        if($item["user"]!=$_SESSION["user"]->getId()){
                            echo"
                                            <div class='row mt-5'>
                                                <div class='comentario'>
                                                    <div class='col-lg-1'>
                                                        <img class='img-thumbnail rounded-circle img-comentario' src='https://image.flaticon.com/icons/png/512/59/59170.png'>  
                                                    </div>
                                                    <div class='col-lg-11'>
                                                        <h5>{$nomeUsuario["nome"]}</h5>                                        
                                                        <p>{$item["comentario"]}</p>
                                                    </div>
                                                </div>
                                            </div>
                            ";
                        }else{
                            echo"
                                            <div class='row mt-5'>
                                                <div class='comentario'>
                                                    <div class='col-lg-1'>
                                                        <img class='img-thumbnail rounded-circle img-comentario' src='https://image.flaticon.com/icons/png/512/59/59170.png'>  
                                                    </div>
                                                    <div class='col-lg-11'>
                                                        <h5>{$nomeUsuario["nome"]}</h5>                                        
                                                        <p>{$item["comentario"]}</p>
                                                        <a href='../controle/deleteComent.php?id={$item["id"]}'>Apagar comentário</a>
                                                    </div>
                                                </div>
                                            </div>
                            ";
                        }
                    }
                }
                echo"
                            </div>
                        </div>
                    </div>
                ";
            }
            unset($key);
        }
    }
}
?>