<?php
require_once("../controle/ControleComentario.php");
require_once("../controle/ControleConteudo.php");
require_once("../controle/ControleUsuario.php");
require_once("../modelo/Turma.php");
session_start();
$conteudo=new Conteudo();
$controle=new ControleConteudo();
$controleUsuario=new ControleUsuario();
$conteudo->setCurso($_SESSION["turma"]->getCurso());
$conteudo->setVideo(1);
echo "
<!DOCTYPE html>
<html lang='pt-br'>
    <head>
        <meta name='viewport' content='width=device-width, initial-scale=1'>    
        <link rel='stylesheet' type='text/css' href='css/video.css'>
        <!-- jQuery library -->
        <script src='https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js'></script>
        <script src='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js' integrity='sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW' crossorigin='anonymous'></script>        
        <title>Videos | World Dev</title>
        <link rel='icon' href='imagens/vide.png'>
    </head>
    <body style='background-color: #e5eefa';>
    <header class='d-flex flex-column flex-md-row align-items-center p-lg-4 p-md-3 p-sm-3 px-md-4 bg-white border-bottom shadow-sm' style='position: fixed; width: 100%; z-index: 1'>
        <p class='h5 my-lg-0 me-md-auto fw-normal mt-4'><img src='imagens/logoV.png' width='120'></p>
        <nav class='my-2 my-md-0 me-md-3'>            
            <a class='p-2 text-dark' href='Principal.php'>Inicio</a>                                                
            <a class='p-2 text-dark' href='Conf.php'>Configurações</a>
            <a class='p-2 text-dark' href='Ajuda.php'>Ajuda</a> 
            <a class='p-2 text-dark' href='../controle/sair.php'>Sair</a>                        
        </nav>                    
    </header> 
        
";
if($conteudo->getCurso()!=3){
    //Selecionar porCursoC
    $conteudo=$controle->selecionarPorCursoC($conteudo);
    if($conteudo!=null){
        if(!isset($_SESSION["videoInicial"])){
        echo"
    //videos
    <div class='container corpo'>
    <br><br>
        <div class='row col-lg-12'>
            <div class='col-lg-9 col-xs-12'>
                <iframe class='embed-responsive-item video' src='../controle/download.php?nome={$conteudo[0]->getNome()}' allowfullscreen> </iframe>    
            </div>
            <div class='col-lg-3 col-xs-12' >
                <div class='card' id='vide'>
                    <div class='card-body' id='visu'>
                        <ul>
        ";
        $_SESSION["videoInicial"]="";
        }else{
            echo"
    //videos
    <div class='container corpo'>
    <br><br>
        <div class='row col-lg-12'>
            <div class='col-lg-9 col-xs-12'>
                <iframe class='embed-responsive-item video' src='../controle/download.php?nome={$_SESSION["videoInicial"]}' allowfullscreen> </iframe>    
            </div>
            <div class='col-lg-3 col-xs-12' >
                <div class='card' id='vide'>
                    <div class='card-body' id='visu'>
                        <ul>
        ";
        }
    }
    foreach($conteudo as $item){
        $titulo=substr($item->getNome(),0,-4);
        if($item->getNome()==$_SESSION["videoInicial"]){
            echo"
                            <a href='../controle/troca.php?nome={$item->getNome()}'>
                                <li id='azul' style='background-color: #81acef; width: 100%'>                                    
                                    <img class='img-fluid' src='imagens/logoV.png' width='100'>
                                    {$titulo}                                    
                                </li>
                            </a>
            ";
        }else{
            echo"
                            <a href='../controle/troca.php?nome={$item->getNome()}'>
                                <li style='background-color: #fff; width: 100%'>                                    
                                    <img class='img-fluid' src='imagens/logoV.png' width='100'>
                                    {$titulo}                                    
                                </li>
                            </a>
            ";
        }
    }
}else{
    //Selecionar tudo de video
    $conteudo=$controle->selecionarTodos();
    foreach($conteudo as $item){
        if($item->getVideo()==1){
            $videoInical=$item;
            $key=true;
            break;
        }
    }
    if(isset($key)){
        if(!isset($_SESSION["videoInicial"])){
        echo"
            
    //videos
    <div class='container corpo'>
    <br><br>
        <div class='row col-lg-12'>
            <div class='col-lg-9 col-xs-12'>
                <iframe class='embed-responsive-item video' src='../controle/download.php?nome={$videoInical->getNome()}' allowfullscreen> </iframe>
            </div>
            <div class='col-lg-3 col-xs-12' >
                <div class='card' id='vide'>
                    <div class='card-body' id='visu'>
                        <ul>
    ";
            $_SESSION["videoInicial"]="";
        }else{
            echo"
            //videos
            <div class='container corpo'>
            <br><br>
                <div class='row col-lg-12'>
                    <div class='col-lg-9 col-xs-12'>
                        <iframe class='embed-responsive-item video' src='../controle/download.php?nome={$_SESSION["videoInicial"]}' allowfullscreen> </iframe>    
                    </div>
                    <div class='col-lg-3 col-xs-12' >
                        <div class='card' id='vide'>
                            <div class='card-body' id='visu'>
                                <ul>
                ";
        }
        unset($key);
    }
    foreach($conteudo as $item){
        if($item->getVideo()==1){
        $titulo=substr($item->getNome(),0,-4);
            if($item->getCurso()==1){
                $curso="LP";
            }else{
                $curso="HTML/CSS";
            }
            if($item->getNome()==$_SESSION["videoInicial"]){
                echo"
                                <a href='../controle/troca.php?nome={$item->getNome()}'>
                                    <li style='background-color: #81acef; width: 100%'>                                    
                                        <img class='img-fluid' src='imagens/logoV.png' width='100'>
                                        {$curso} {$titulo}                                    
                                    </li>
                                </a>
                ";
            }else{
                echo"
                                <a href='../controle/troca.php?nome={$item->getNome()}'>
                                    <li style='background-color: #fff; width: 100%'>                                    
                                        <img class='img-fluid' src='imagens/logoV.png' width='100'>
                                        {$curso} {$titulo}                                    
                                    </li>
                                </a>
                ";
            }  
        }
    }
}
echo"
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
";
echo"
            </div>
        </div>
";
require_once("Comentarios.php");
echo"
            <footer id='footer' class='mt-5'>
                <div class='container'>
                    <div class='row'>
                        <div class='col-lg-4'>
                            <h5 class='text-dark'>Esperamos por você</h5>
                            <p class='text-dark'>Agradecemos sua presença, não deixe de fazer parte dessa experiência única. Com tudo organizado para você
                            apenas fazer bom uso, sucesso!
                        </div>
                        <div class='offset-lg-3 col text-dark'>
                            <h5>Quer saber mais?</h5>
                            <p><a class='text-dark' href='#'>Quem somos?</a></p>
                            <p><a  class='text-dark'href='#'>Qual nosso intuito?</a></p>
            
                        </div>
                        <div class='col text-dark'>
                            <h5>Entre em contato</h5>
                            <p>
                            <img src='imagens/insta.png' width='24'>
                            <a href='https://www.instagram.com/world_dev.proj/' target='_blank'>@world_dev.proj</a></p>
                            <p><img src='imagens/mail.png'>
                            <a href='https://mail.google.com/mail/u/0/' target='_blank'>worlddevproj@gmail.com</a></p>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        <script src='js/sweetAlert.js'></script>
";
if(isset($_SESSION["erroComentario"])){
    echo"<script>swal('Ok','Comentário adicionado','success');</script>";
    unset($_SESSION["erroComentario"]);
}
echo"
    </body>
</html>
";
if(isset($_SESSION["videoInicial"])){
    unset($_SESSION["videoInicial"]);
}
?>
