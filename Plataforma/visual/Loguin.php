<?php
echo"
<!DOCTYPE html>
<html>
<head>
   <title>Plataforma | Projeto Social</title>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>    
    <link rel='stylesheet' href='css/bootstrap.css'>
    <link rel='stylesheet' href='css/login.css'>
    <link rel='icon' href='imagens/logo.png'>
</head>
<body>
  <div class='row'>    
    <div class='col-md-6 vertical-align'>
      <div class='posição'>
        <center>
           <h4 class='font-italic' style='background-color: #fff'>Login</h4>
          <br>
          <div class='col-md-7'>
            <form class='form vertical-alignC' action='../controle/verificar.php'  method='post'>
              <div class='form-group'>
                <input type='email' name='email' class='form-control' id='email' aria-describedby='emailHelp' placeholder='Seu email' required>
                <br>
                <a href='#'><span id='but'>
                  <img src='imagens/senha.png' id='img' style='display: none'>
                </span></a>
                <input id='sen' type='password' name='senha' class='form-control' aria-describedby='emailHelp' placeholder='Senha' required> 
                <br><br>
                <center>
                  <input type='submit' value='Entrar' id='hov' class='btn btn-outline-secondary' tabindex='-1' aria-disabled='true'/>
                </center>
                <br>
                <small>Ainda não se cadastrou?</small><a href='Cadastro.php' style='text-decoration: none'> cadastre-se</a>
              </div>
            </form>
          </div>
        </center>
      </div>
    </div>
  </div>
</body>
<script src='js/bootstrap.min.js' integrity='sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy' crossorigin='anonymous'></script>
<script src='js/jquery-3.4.1.min.js'></script>
<script src='js/test.js'></script>


</html>
";

?>
