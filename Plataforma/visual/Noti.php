<?php
echo "
<!DOCTYPE html>
<html lang='pt-br'>
<head>
    <meta name='viewport' content='width=device-width, initial-scale=1'>    
    <link rel='stylesheet' type='text/css' href='css/noti.css'>
    <title>Notificações | World Dev</title>
</head>
<body>

    <header class='mb-5 d-flex flex-column flex-md-row align-items-center p-lg-4 p-md-3 p-sm-3 px-md-4 bg-white border-bottom shadow-sm' style='position: fixed; width: 100%'>
        <p class='h5 my-sm-3 my-lg-0 me-md-auto fw-normal'>World Dev</p>        
        <nav class='my-2 my-md-0 me-md-3'>            
            <a class='p-2 text-dark' href='Principal.php'>Inicio</a>
            <a class='p-2 text-dark' href='Videos.php'>Aulas</a>
            <a class='p-2 text-dark' href='Conteudos.php'>Conteúdos</a>
            <a class='p-2 text-dark' href='../controle/sair.php'>Sair</a>                        
        </nav>                    
    </header>

    <div class='container'>
    <br><br><br><br><br>
        <div class='row'>
            <div class='col-lg-12'>
            <div class='alert alert-warning alert-dismissible fade show' role='alert'>
            <strong>Oloco, meu AULAAAAAAAA!!</strong> AULAAAAAAAA!
            <button type='button' class='close' data-dismiss='alert' aria-label='Close justify-content-end'>
            <span aria-hidden='true'>&times;</span>
            </button>
            </div>
           

            <div class='col-lg-12'>
                <p  class=''style='font-size: 40px; color:#3b3b3b '>Horário de Html/Css</p>
                
            </div>
            <br></br>
            <table class='table  table-hover'>
  <thead>
    <tr>
      <th scope='col'>Dia</th>
      <th scope='col'>Horário</th>
      <th scope='col'>Conteúdo</th>
      <th scope='col'>Professoras</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope='row'>08/01</th>
      <td>08:00 hrs ás 09:00 hrs</td>
      <td>Introdução  e Fundamentos Html</td>
      <td>Eduarda</td>
      
      
    </tr>
    <tr>
      <th scope='row'>09/01</th>
      <td>14:00 hrs ás 15:00 hrs</td>
      <td>Elementos do texto</td>
      <td>Kailane</td>
    </tr>

    <tr>
      <th scope='row'>10/01</th>
      <td>08:00 hrs ás 09:00 hrs</td>
      <td>Hyperlinks e links </td>
      <td>Kailane</td>
    </tr>
    <tr>
    <th scope='row'>11/01</th>
    <td>14:00 hrs ás 15:00 hrs</td>
    <td>Tag´s + Resolução de Exercícios</td>
    <td>Eduarda</td>
  </tr>
  <tr>
  <th scope='row'>12/01</th>
  <td>08:00 hrs ás 09:00 hrs</td>
  <td>Imagens + Resolução de exercícios</td>
  <td>Eduarda</td>
</tr>
<tr>
<th scope='row'>15/01</th>
<td>14:00 hrs ás 15:00 hrs</td>
<td>Formulários</td>
<td>Kailane</td>
</tr>
<tr>
<th scope='row'>16/01</th>
<td>08:00 hrs ás 09:00 hrs</td>
<td>Atributos</td>
<td>Kailane</td>
</tr>
<tr>
<th scope='row'>17/01</th>
<td>14:00 hrs ás 15:00 hrs</td>
<td>Introdução e Fundamentos CSS + Resolução de Exercícios</td>
<td>Eduarda</td>
</tr>
<tr>
<th scope='row'>18/01</th>
<td>08:00 hrs ás 09:00 hrs</td>
<td> Seletores + Resolução de Exercícios</td>
<td>Eduarda</td>
</tr>
<tr>
<th scope='row'>19/01</th>
<td>14:00 hrs ás 15:00 hrs</td>
<td>Elementos CSS, formação de cores</td>
<td>Kailane</td>
</tr>
  </tbody>
</table>
        </div>
</div>
<br></br>


<div class='col-lg-12'>
<p  class=''style='font-size: 40px; color:#3b3b3b '>Horário de Lógica de Programação</p>

</div>
<br></br>
<table class='table table-hover'>
<thead>
<tr>
<th scope='col'>Dia</th>
<th scope='col'>Horário</th>
<th scope='col'>Conteúdo</th>
<th scope='col'>Professores</th>
</tr>
</thead>
<tbody>
<tr>
<th scope='row'>09/01</th>
<td>07:00 hrs ás 08:00 hrs</td>
<td>Introdução e Fundamentos da Lógica de Programação</td>
<td>Kauan Cordeiro</td>


</tr>
<tr>
<th scope='row'>09/01</th>
<td>08:00 hrs ás 09:00 hrs</td>
<td>Linguagens compiladas, interpretadas e híbridas</td>
<td>Isabel</td>
</tr>

<tr>
<th scope='row'>09/01</th>
<td>09 hrs ás 09:30 hrs</td>
<td> Entrada e saída de dados </td>
<td>Isabel</td>
</tr>
<tr>
<th scope='row'>09/01</th>
<td>09:30 hrs ás 10:00 hrs</td>
<td>Variáveis e suas definições.Tipos primitivos:Nome e Valor</td>
<td>Kauan Cordeiro</td>
</tr>
<tr>
<th scope='row'>10/01</th>
<td>13:30 hrs ás 15:30 hrs</td>
<td>Resolução de Exercícios</td>
<td>Emanuel</td>
</tr>
<tr>
<th scope='row'>11/01</th>
<td>08:00 hrs ás 09:00 hrs</td>
<td> Estrutura básica do Java e Operadores matemáticos na lógica de programação e tabela verdade.</td>
<td>Kauan Cordeiro</td>
</tr>
<tr>
<th scope='row'>11/01</th>
<td>14:00 hrs ás 15:00 hrs</td>
<td>Resolução de Exercícios</td>
<td>Emanuel</td>
</tr>
<tr>
<th scope='row'>12/01</th>
<td>07:00 hrs ás 08:00 hrs</td>
<td>Estrutura condicional simples</td>
<td>Isabel</td>
</tr>
<tr>
<th scope='row'>12/01</th>
<td>08:00 hrs ás 10:00 hrs</td>
<td>Estruturas condicionais aninhada e composta</td>
<td>Kauan cordeiro</td>
</tr>
<tr>

<th scope='row'>15/01</th>
<td>14:00 hrs ás 15:00 hrs</td>
<td>Operadores matemáticos na lógica de programação e tabela verdade</td>
<td>Isabel</td>
</tr>
<tr>
<th scope='row'>16/01</th>
<td>08:00 hrs ás 09:00 hrs</td>
<td>Resolução de Exercícios</td>
<td>Emanuel</td>
</tr>
<th scope='row'>17/01</th>
<td>08:00 hrs ás 09:00 hrs</td>
<td>Estrutura de repetição for e Estruturas de repetição While e Do While</td>
<td>Isabel</td>
</tr>
<th scope='row'>18/01</th>
<td>08:00 hrs ás 10:00 hrs</td>
<td>Arrays, matrizes e Métodos  </td>
<td>Isabel</td>
</tr>
<th scope='row'>18/01</th>
<td>14:00 hrs ás 15:00 hrs</td>
<td>Resolução de Exercícios  </td>
<td>Emanuel</td>
</tr>
<th scope='row'>19/01</th>
<td>08:00 hrs ás 09:00 hrs</td>
<td>Resolução de Exercícios</td>
<td>Emanuel</td>
</tr>
</tr>
</tbody>
</table>
</div>
</div>
</div>
        
</body>
</html>
";
?>
