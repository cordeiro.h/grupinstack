<?php
require_once("../modelo/Usuario.php");
require_once("../modelo/Turma.php");
session_start();
echo "
<!DOCTYPE html>
<html lang='pt-br'>
<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
    <title>World Dev</title>             
    <link rel='stylesheet' type='text/css' href='css/princip.css'>
    <link rel='icon' href='imagens/logo.png'>
</head>
<body style='background: #e5eefa'>
";
echo '
<div class="wrapper">
    <!-- Sidebar -->
    <nav id="sidebar">
        <div class="sidebar-header">
            <h3>World Dev</h3>
        </div>

        <ul class="list-unstyled components">
';
            echo"
            
            <div class='circle-image'>
                <img src='imagens/imaa.jpg'/>
            </div>
                            
            <p>{$_SESSION['user']->getNome()}</p>
            
            ";
if($_SESSION["turma"]->getCurso()!=0){
            if($_SESSION["user"]->getAdm()==0){
echo'
                <li>
                <a href="Videos.php">Aulas</a>
            </li>
            <li>
';
            }else{
echo'
                <li>
                <a href="VideosA.php">Aulas</a>
            </li>
            <li>
';
            }
}else{
    echo"<li>";
}
            if(intval($_SESSION["user"]->getAdm())==0){
                echo'<a href="Conteudos.php" title="Exercícios e Slides">Conteúdos</a>';
            }else{
                echo'<a href="ConteudosA.php" title="Exercícios e Slides">Conteúdos</a>';
            }
echo'  
            </li>
            <li>
                <a href="Noti.php">Horário</a>
            </li>
            <li>
                <a href="Blog.php">Blog</a>
            </li> 
            <li class="active">
                <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Mais</a>
                <ul class="collapse list-unstyled" id="homeSubmenu">
                    <li>                        
                        <a href="Conf.php">
                            <img src="imagens/conf.webp">  Configurações
                        </a>
                    </li>
                    
                    <li>
                        <a href="Noti.php"><img src="imagens/not.png">Notificações</a>
                    </li>
                    <li>
                        <a href="../controle/sair.php"><img src="imagens/sair.png">Sair</a>
                    </li>
                </ul>
            </li>              
        </ul>
    </nav>

    <div id="content">            
        <div class="container-fluid">
            <button type="button" id="sidebarCollapse" class="btn p-0" title="Menu">
                <img src="imagens/menu.png" style="border: 1px solid #2d64b3; border-radius: 10px; padding: 10px">                        
            </button>                                
        </div                    
        </div>                   
        <hr>
        <div class="my-3 p-3 rounded shadow-sm" style="background: #fbfcfe">
            <div class="container">
';
            if(intval($_SESSION["turma"]->getCurso())==3 || intval($_SESSION["turma"]->getCurso()==0)){
                echo'<h2 class="text-center mb-5">Html/css e LP</h2>';
            }
echo'
                <div class="row offset-lg-1 mb-5">
                    <div class="col-lg-7" style="text-align: justify; margin: auto 0 auto 0">
                        <div class="alert alert-primary" role="alert" style="width: auto !important">
                            Estamos gratos por sua presença! Que felicidade em saber que você tem tanta vontade de aprender! 
                            Que tal saber um pouco mais das duas disciplinas que você resolveu escolher para se sentir
                            mais motivado em?
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <img src="imagens/bob.gif" style="border-radius: 10px">
                    </div>
                </div>
';
            if(intval($_SESSION["turma"]->getCurso())==1 || intval($_SESSION["turma"]->getCurso())==0 || intval($_SESSION["turma"]->getCurso())==3){
                echo'
                <hr>
                <div class="row offset-lg-1">                
                    <div class="col-lg-3">
                        <img src="imagens/lps.svg" style="border-radius: 10px" class="img-fluid mt-3 mb-3">
                    </div>
                    <div class="col-lg-7" style="text-align: justify;">
                        <div class="alert alert-primary" role="alert">
                            Lógica de programação
                            <hr>
                            Todas as escolhas ou ações que fazemos durante o dia dependem da nossa capacidade de raciocinar logicamente, e na programação não é diferente. A disciplina de LP é destinada à transmitir conhecimentos fundamentais para programar em 
qualquer linguagem e à desenvolver o pensamento racional voltado para a criação de programas computacionais.
                        </div>
                    </div>
                </div>
                ';
            }
            if(intval($_SESSION["turma"]->getCurso())==2 || intval($_SESSION["turma"]->getCurso())==0 || intval($_SESSION["turma"]->getCurso())==3){
                    echo'
                    <div class="row offset-lg-1">                
                        <div class="col-lg-3">
                            <img src="imagens/hts.jpg" style="border-radius: 10px;" class="img-fluid mt-3 mb-3">
                        </div>
                        <div class="col-lg-7" style="text-align: justify;">
                            <div class="alert alert-primary" role="alert">
                                HTML/CSS
                                <hr>
                                HTML/CSS são a base de todo desenvolvimento web. Em nosso curso abordaremos o básico dessas disciplinas de forma simplificada e descontraída para você aprender e desenvolver suas primeiras páginas para a web.
                            </div>
                        </div>
                    </div>
                    ';
            }
echo'
            </div>
        </div>
        
    </div>    
';  
echo"
    <script src='https://code.jquery.com/jquery-3.3.1.slim.min.js' integrity='sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo' crossorigin='anonymous'></script>           
    <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js' integrity='sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm' crossorigin='anonymous'></script>    
    <script src='https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js'></script>
    <script src='js/prin.js'></script>
</body>
</html>
";
?>