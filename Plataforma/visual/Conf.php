<?php
require_once("../modelo/Turma.php");
require_once("../modelo/Usuario.php");
session_start();
if(isset($_SESSION["user"])){
//Lembrar de tratar os erros session
echo "
<!DOCTYPE html>
<html lang='pt-br'>
<head>
    <meta name='viewport' content='width=device-width, initial-scale=1'>    
    <link rel='stylesheet' type='text/css' href='css/gerall.css'>
    <link rel='icon' href='imagens/conf.webp'>
    <title>Configuração | World Dev</title>    
</head>
<body style='background-color: #1a3966'>
    <header class='d-flex flex-column flex-md-row align-items-center p-lg-4 p-md-3 p-sm-3 px-md-4 bg-white border-bottom shadow-sm' style='position: fixed; width: 100%; margin-bottom: 10%; z-index: 1'>
        <p class='h5 my-lg-0 me-md-auto fw-normal mt-4'><img src='imagens/logoV.png' width='120'></p>
        <nav class='my-2 my-md-0 me-md-3'>            
            <a class='p-2 text-dark' href='Principal.php'>Inicio</a>
            <a class='p-2 text-dark' href='Ajuda.php'>Ajuda</a>
            <a class='p-2 text-dark' href='../controle/sair.php'>Sair</a>
        </nav>                    
    </header>
    <br><br>
    <div class='container mt-5 bg-white'>      

        <div class='row'>
            <img src='imagens/conf.jpg' class='img-fluid'>
            <p style='font-size: 40px; color:#384b66' class='text-center'>Configurações de Usuário</p>
            <hr class='mb-5' style='color: #384b66'>
        </div>
        <div class='row'>
            <div class='col-lg-2 offset-lg-3 mb-5' >
                <div class='circle-image' id='img'>
                    <img src='../controle/imgPerfil.php?id={$_SESSION['user']->getId()}'/>
                    <img src='imagens/imaa.jpg'/>
                </div>
            </div>            
            <div class='col-lg-4 card p-5'>
                <p style='font-size: 20px'>Alterar imagem de perfil</p>
                <form action='../controle/inserirImg.php' method='post' enctype='multipart/form-data'>
                    <div class='form-group'>
                        <label id='nameI'> </label>
                        <label class='mt-2 mb-1 btn bg-dark' id='inp' for='anexar'>Selecionar arquivo</label>                                    
                        <input type='file' id='anexar' name='img'required/>                         
                        <input style='color: #fff; width: 100%' class='btn bg-dark mt-3' type='submit' value='Alterar imagem' />
                    </div>                    
                </form>
            </div>
        </div>
"; 
echo"            
                        
        <form action='../controle/atualizarC.php' method='post'>
            <div class='row' id='centro'>                            
                    <div class='form-group'>
                        <label class='mb-2' for='nome' style='font-size: 20px'>Nome:</label>
                        <input class='mb-2 form-control' id='nome' type='text' name='nome' value='{$_SESSION["user"]->getNome()}' placeholder='Nome ou apelido'/>
                    </div>                                      
                    <div class='form-group'>                        
                        <label class='mb-2' for='text' style='font-size: 20px'>Nova Senha:</label>
                        <input class='mb-2 form-control' type='password' name='senha' id='text' rows='3' placeholder='Alterar senha' />
                    </div>          
                    <br>
";
                if($_SESSION['turma']->getCurso()!=0){
echo"
                    <div class='form-group'>                        
                        <label class='mb-2' for='text' style='font-size: 20px'>Curso:</label>
                        <select class='form-control' name='curso' id='nome'>                                        
";
                        if($_SESSION['turma']->getCurso()==1){
                            echo"
                                <option value='1' selected>LP</option>
                                <option value='2'>HTML/CSS</option>
                                <option value='3'>Duas disciplinas</option>
                            ";
                        }else if($_SESSION['turma']->getCurso()==2){
                            echo"
                                <option value='2' selected>HTML/CSS</option>
                                <option value='1'>LP</option>
                                <option value='3'>Duas disciplinas</option>
                            ";
                        }else{
                            echo"
                                <option value='3' selected>Duas disciplinas</option>
                                <option value='2'>HTML/CSS</option>
                                <option value='1'>LP</option>
                            ";
                }
                            
echo"
                        </select>
                    </div>                           
";
                }
echo"                                                      
            </div>         
                <div class='text-center'>
                    <input class='btn mb-2' style='background-color: #1a3966;color: #fff;'  type='submit' value='Alterar dados'>                  
                </div>
                <br>       
                <div class='row'>
                    <div class='bg-dark' style='color: #fff'>
                        <p class='text-center mt-2'>Obrigada por escolher a Wolrd Dev</p>
                    </div>
                </div>                
            </form>  
        </div>        
    <div class='container mt-5'>
";
if($_SESSION["user"]->getAdm()==0){
    echo"
            <a href='#' class='btn' title='Tem certeza que quer nos abandonar?' id='del'>Deletar conta</a>    
    ";
}
echo"
    </div>    
    <br> <br>
    <script src='js/jquery-3.4.1.min.js'></script>
    <script src='js/sweetAlert.js'></script>
    <script src='js/del.js'></script>
";
if(isset($_SESSION["erroDel"])){
    if($_SESSION["erroDel"]){
        echo "<script>swal('Ops','Não foi possivel deletar sua conta por falhas técnicas','error');</script>";
        unset($_SESSION["erroDel"]);
    }
}
if(isset($_SESSION["erroImg"])){
    if($_SESSION["erroImg"]){
        echo "<script>swal('Ops','Não foi possivel alterar sua imagem de perfil','error');</script>";
        unset($_SESSION["erroImg"]);
    }
}
if(isset($_SESSION["erroUpdate"])){
    if($_SESSION["erroUpdate"]){
        echo"<script>swal('Ops','Não foi possível fazer a atualização dos dados','error');</script>";
    }else{
        echo"<script>swal('OK!','Dados atualizados com sucesso','success');</script>";
    }
    unset($_SESSION["erroUpdate"]);
}
if(isset($_SESSION["insert"])){
    echo"<script>swal('OK!','Dados atualizados com sucesso','success');</script>";
    unset($_SESSION["insert"]);
}
echo"
</body>
</html>";
}else{
    session_destroy();
    header("Location: ../");
}
?>
