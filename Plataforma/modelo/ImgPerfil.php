<?php
class ImgPerfil{
        private $id;
        private $arquivo;
        private $nome;
        private $extensao;
        private $user;

        public function getId(){
            return $this->id;
        }
        public function setId($i){
            $this->id = $i;
        }
        public function setArquivo($arquivo){
            $this->arquivo=$arquivo;
        }
        public function getArquivo(){
            return $this->arquivo;
        }
        public function setNome($nome){
            $this->nome=$nome;
        }
        public function getNome(){
            return $this->nome;
        }
        public function setExtensao($extensao){
            $this->extensao=$extensao;
        }
        public function getExtensao(){
            return $this->extensao;            
        }
        public function setUser($user){
            $this->user=$user;
        }
        public function getUser(){
            return $this->user;            
        }
        
    }
?>