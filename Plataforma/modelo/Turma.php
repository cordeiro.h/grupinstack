<?php
    require_once("Usuario.php");
    class Turma{
        private $id;
        private $curso;
        public $user;
        function __construct(){
            $this->user=new Usuario();
        }
        public function setId($id){
            $this->id=$id;
        }
        public function getId(){
            return $this->id;
        }
        public function setCurso($curso){
            $this->curso=$curso;
        }
        public function getCurso(){
            return $this->curso;
        }
    }
?>
