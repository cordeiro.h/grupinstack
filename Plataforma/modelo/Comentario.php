<?php
require_once("Usuario.php");
require_once("Conteudo.php");
class Comentario{
    private $id;
    private $comentario;
    public $user;
    public $video;    
    function __construct(){
        $this->user = new Usuario();
        $this->video = new Conteudo();
    }
    public function getId(){
        return $this->id;
    }
    public function setId($i){
        $this->id = $i;
    }
    public function getComentario(){
        return $this->comentario;
    }
    public function setComentario($comentario){
        $this->comentario = $comentario;
    }
}
?>
