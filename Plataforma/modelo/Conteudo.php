<?php
    class Conteudo{
        private $id;
        private $arquivo;
        private $nome;
        private $extensao;
        private $curso;
        private $video;
        private $data;
        public function setId($id){
            $this->id=$id;
        }
        public function getId(){
            return $this->id;
        }
        public function setArquivo($arquivo){
            $this->arquivo=$arquivo;
        }
        public function getArquivo(){
            return $this->arquivo;
        }
        public function setNome($nome){
            $this->nome=$nome;
        }
        public function getNome(){
            return $this->nome;
        }
        public function setExtensao($extensao){
            $this->extensao=$extensao;
        }
        public function getExtensao(){
            return $this->extensao;
        }
        public function setCurso($curso){
            $this->curso=$curso;
        }
        public function getCurso(){
            return $this->curso;
        }
        public function setVideo($video){
            $this->video=$video;
        }
        public function getVideo(){
            return $this->video;
        }
        public function setData($data){
            $this->data=$data;
        }
        public function getData(){
            return $this->data;
        }
    }
?>