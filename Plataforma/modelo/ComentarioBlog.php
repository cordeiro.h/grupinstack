<?php
class CmtBlog{
    private $id;
    private $cmtBlog;
    private $user;
    private $email;
    private $post;    
    
    public function getId(){
        return $this->id;
    }
    public function setId($i){
        $this->id = $i;
    }
    public function getCmtBlog(){
        return $this->cmtBlog;
    }
    public function setCmtBlog($cmtBlog){
        $this->cmtBlog = $cmtBlog;
    }
    public function getUser(){
        return $this->user;
    }
    public function setUser($u){
        $this->user = $u;
    }
    public function getPost(){
        return $this->post;
    }
    public function setPost($p){
        $this->post = $p;
    }
}
?>
