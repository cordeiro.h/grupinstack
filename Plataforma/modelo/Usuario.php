<?php
    class Usuario{
        private $id;
        private $nome;
        private $email;
        private $senha;
	    private $adm;
        
        public function getId(){
            return $this->id;
        }
        public function setId($i){
            $this->id = $i;
        }
        public function getNome(){
            return $this->nome;
        }
        public function setNome($n){
            $this->nome = $n;
        }
        public function getEmail(){
            return $this->email;
        }
        public function setEmail($e){
            $this->email = $e;
        }
        public function getSenha(){
            return $this->senha;
        }
        public function setSenha($s){
            $this->senha = $s;
        }
        public function getAdm(){
            return $this->adm;
        }
        public function setAdm($a){
            $this->adm = $a;
        }

    }
?>  
