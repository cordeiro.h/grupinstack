<?php 
	class ModeloPost{
		private $titulo;
		private $autor;
		private $texto;
		private $id;
		private $data;
		private $resumo;


		public function setId($i){
			$this->id=$i;
		}
		public function getId(){
			return $this->id;
		}
		public function setTitulo($ti){
			$this->titulo=$ti;
		}
		public function getTitulo(){
			return $this->titulo;
		}
		public function setAutor($na){
			$this->autor=$na;
		}
		public function getAutor(){
			return $this->autor;
		}
		public function setTexto($te){
			$this->texto=$te;
		}
		public function getTexto(){
			return $this->texto;
		}
		public function setData($d){
			$this->data=$d;
		}
		public function getData(){
			return $this->data;
		}
		public function setResumo($re){
			$this->resumo=$re;
		}
		public function getResumo(){
			return $this->resumo;
		}
	}

 ?>