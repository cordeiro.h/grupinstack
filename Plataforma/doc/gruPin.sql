 DROP SCHEMA IF  EXISTS gruPin;
CREATE SCHEMA IF NOT EXISTS  gruPin; 

CREATE TABLE gruPin.usuario(
	id INT(3) PRIMARY KEY AUTO_INCREMENT,
	nome VARCHAR (100) NOT NULL,
	email VARCHAR(50) UNIQUE NOT NULL,
	senha VARCHAR (32) NOT NULL,
	adm INT(1) NOT NULL
	
);
CREATE TABLE gruPin.conteudo(
	id INT(3) PRIMARY KEY AUTO_INCREMENT,
	arquivo LONGBLOB NOT NULL,
	nome VARCHAR(100) UNIQUE NOT NULL,
	extensao VARCHAR(20) NOT NULL,
	curso INT(1) NOT NULL,
	data VARCHAR(20) NOT NULL,
	video INT(1) NOT NULL
);
CREATE TABLE gruPin.comentario(
	id INT(5) PRIMARY KEY AUTO_INCREMENT,
	comentario LONGTEXT NOT NULL,
	user INT(3) NOT NULL,
	video INT(3) NOT NULL,
	FOREIGN KEY(user) REFERENCES usuario(id),
	FOREIGN KEY(video) REFERENCES conteudo(id)
);
CREATE TABLE gruPin.turma(
	id INT(3) PRIMARY KEY AUTO_INCREMENT,
	curso INT(1) NOT NULL,
	user INT(3) NOT NULL,
	FOREIGN KEY(user) REFERENCES usuario(id)
);

CREATE TABLE gruPin.post(
    id INT PRIMARY KEY AUTO_INCREMENT,
    titulo VARCHAR(100)  NOT NULL,
    autor VARCHAR(100)  NOT NULL,
    texto LONGTEXT NOT NULL,
    data VARCHAR(100)  NOT NULL,
    resumo VARCHAR(100)  NOT NULL

);
CREATE TABLE gruPin.imagem(
    id INT PRIMARY KEY AUTO_INCREMENT,
    nome LONGTEXT  NOT NULL,
    tipo VARCHAR(100) NOT NULL,
    dados LONGBLOB NOT NULL,
    post INT(4) NOT NULL,
    FOREIGN KEY(post) REFERENCES post(id)
);
CREATE TABLE gruPin.imgPerfil(
    id INT(3) PRIMARY KEY AUTO_INCREMENT,
    arquivo LONGBLOB NOT NULL,
    nome VARCHAR(100) NOT NULL,
    extensao VARCHAR(20) NOT NULL,
    user INT(3) NOT NULL,
    FOREIGN KEY(user) REFERENCES usuario(id)
);
CREATE TABLE gruPin.cmtBlog(
	id INT(5) PRIMARY KEY AUTO_INCREMENT,
	cmtBlog LONGTEXT NOT NULL,
	user INT(3) NOT NULL,
	post INT(3) NOT NULL,
	FOREIGN KEY(user) REFERENCES usuario(id),
	FOREIGN KEY(post) REFERENCES post(id)
);

INSERT INTO gruPin.usuario(nome,email,senha,adm) VALUES("GruPin","grupinstack@gmail.com","c7870ff82d61a5705d2ea6db4fa53cff",1),("Nathalya","nathalya.gds@gmail.com","e62df2f3ebc887b43bcbc20c53187c30",0),("Érika","erikalima8755@gmail.com","0aa15b49fdaade39512bbd39b3eca3c2",0),("R. Reis","rodrigoecia2002@gmail.com","8a20e6eb97110d8fc2cfb847c69d7f9e",0),("Carol","ana.lima644@aluno.ce.gov.br","3590e11e1787fa91af0923d5b8d6cac6",0),("Miqlu","SampaioMiguel974@gmail.com","a4fb8f9eda935e5782241b79a6a58cef",0),("Paulo","paulojs344@gmail.com","ce3189390fd96ed0a3ea8b70d073bf5d",0),("ISABELE CELINE MATOS DA SILVA","isabeleceline01@gmail.com","834c7e638ffd564c24ea1bfb9fff1bf2",0),("Ingrid","ingridbarbosa20155@gmail.com","2dbc1ef74330157c8749f6ebcf2639c9",0),("Amanda Victória","amandavictoria0308@gmail.com","037d12d9988332b663cc655773f2499a",0),("Majuu","juliasousafernandes1@gmail.com","04305e8ef1c15dbf249cc0c99ce86107",0),("Dante","Darkdanterof@gmail.com","e702fde75ccc9ea11f0972be0e94951c",0),("Ramiro","ramirowesley@24gmail.com","a07ebade7cfcf27c4ae0d230ba4659f0",0),("Izadora","iizatavares001@gmail.com","1577db3682ae9e707d368ea80c18dfec",0),("Jeff","jeffbrito776@gmail.com","e10adc3949ba59abbe56e057f20f883e",0),("Diego","diegomarcelosou@gmail.com","157d07c0962e7fab86cd422af749d01e",0),("Thiago","to96201@gmail.com","01bfbbcdb734c1653886d9d5fcecbc44",0),("Dante","Darkdanteof@gmail.com","e702fde75ccc9ea11f0972be0e94951c",0),("Yasmin Keury","yasmimkeury07@gmail.com","d2ea99e13b322a47dcd2e15d302fe29b",0),("Rafael","francisco.rafael.rm@gmail.com","8e0018a76421500f7026fae0d4bba9d1",0),("Edson","raimundoedsonmirandadossantos@gmail.com","15e66799196c64faaf7cf3dc9014e705",0),("joão pedro","jpmalves54@gmail.com","994d3759d5eb2e49ac553f0047762b5d",0),("Sibelly","sibellyg56@gmail.com","dbbec288282c76bb919d6b28a54d929f",0),("Antony","antony.caiquec@gmail.com","e197f5bc7d1ea42881716fa86d296960",0),("Eduardo","ce917789@gmail.com","49850d451e21883885d32c41395a1771",0),("Carlos","ce737349@gmail.com","49850d451e21883885d32c41395a1771",0),("Mestre dos magros feio cagão","romuloferreirasales@gmail.com","19e91ac1402fd909a5d87c7c31eb377b",0),("Venancio","lucasvenancio2011@gmail.com","25d55ad283aa400af464c76d713c07ad",0),("Isabela Sousa","Isabelasousa0517@gmail.com","3efa4ccb4dcba62b4c6bcbf14e8e311d",0),("Gisele","giseleumbelino1238@gmail.com","d0b2fde9e9d414a2361b784e9b00ee60",0),("Daniela Souza Santos","danielasouzasantos554@gmail.com","0a6bb7151e64a18c572995b02ecc6750",0),("Kauane","anakauanelins7@gmail.com","c41594c1c536c66a9ee9c348e3b2601a",0),("Crisnaelly","naely186cris@gmail.com","0d2c260ea50b5190eb8f243c31c856b9",0),("Érika","erikalima8700@gmail.com","0aa15b49fdaade39512bbd39b3eca3c2",0),("eduarda","mn11112004@gmail.com","23dda1033b794bcc5a9d8fa9ea3a3256",0),("Eduardo","edu.ss2005dez09@gmail.com","0d9f8af3c4d209c58acfa7fbb95bc597",0),("Samuel","skx23nerd@gmail.com","206627b64c78689a961b2fc8026fdace",0),("Giorno Giovanna","giovannibarros750@gmail.com","90f397cf495cd8d98f582223409f20a9",0),("Eduarda","meuryeduarda12345@gmail.com","f4136992608f32cf8c41cbdacee74b73",0),("Andrielle","monteiroandrielle18@gmail.com","328ff730c6c07f4f345554cb11c2ca11",0),("Camilli","eg4864195@gmail.com","488f9c601d323e1f20091c7241d88a6a",0),("Jorge","jorge_jdarcoverde@hotmail.com","e120ea280aa50693d5568d0071456460",0);
INSERT INTO gruPin.turma(curso,user) VALUES(3,1),(3,2),(1,3),(3,4),(3,5),(3,6),(1,7),(3,8),(2,9),(1,10),(1,11),(3,12),(3,13),(3,14),(1,15),(3,16),(3,17),(3,18),(3,19),(3,20),(1,21),(1,22),(3,23),(3,24),(0,25),(1,26),(3,27),(1,28),(1,29),(3,30),(1,31),(1,32),(1,33),(3,34),(1,35),(3,36),(1,37),(3,38),(3,39),(2,40),(1,41),(3,42);

