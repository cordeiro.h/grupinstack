<!--Isabel-->
<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="visual/css/home.css">
        <link rel="icon" href="visual/imagens/logo.png">
        <title>HOME | World Dev</title>
    </head>
<body style="background-color:#fefefe;">
    <header class="d-flex flex-column flex-md-row align-items-center p-lg-4 p-md-3 p-sm-3 px-md-4 bg-white border-bottom shadow-sm" style="position: fixed; width: 100%">
        <p class="h5 my-lg-0 me-md-auto fw-normal mt-4"><img src="visual/imagens/logoV.png" width="120"></p>
        <nav class="my-2 my-md-0 me-md-3">
          <a class="p-2 text-dark" href="#">Home</a>
          <a class="p-2 text-dark" href="visual/Cadastro.php">Cadastro</a>
          <a class="p-2 text-dark" href="visual/Loguin.php">Login</a>
          <a class="p-2 text-dark" href="visual/Blog.php">Blog</a>
        </nav>
    </header>
    <section class="section section_welcome">
        <div id="topo">
        </div>
    </section>
    <div class="mt-5" id="preto">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="text-center">
                        <div class="fundoI p-3 center">
                            <img src="visual/imagens/men.png" width="64">
                        </div>
                        <h6>Cursos</h6>
                        <p class="p-2">Nós iremos oferecer duas opções de cursos que são a base para você dar prosseguintem em desenvolvimento
                        web e programação, disponibilizando conteúdos e um método de organização intuitivo para você.
                        </p>
                    </div>
                </div>
                <div class="col">
                    <div class="text-center">
                        <div class="fundoI p-3 center">
                            <img src="visual/imagens/blog.png" width="64">
                        </div>
                        <h6>Blog</h6>
                        <p class="p-2">Em nosso blog iremos apresentar conteudos gerais na área de Informática, fique ligado para estar sempre
                        adquirindo novos conhecimentos.
                        </p>
                    </div>
                </div>
                <div class="col">
                    <div class="text-center">
                        <div class="fundoI p-3 center">
                            <img src="visual/imagens/ok.webp" width="64">
                        </div>
                        <h6>Aulas exclusivas</h6>
                        <p class="p-2">As aulas disponibilizadas serão exclusivamente de quem se inscreveu, portanto, não perca o prazo de inscrição
                        pois isso irá garantir seu total acesso a nossa plataforma.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-dark">.</div>
    <hr class="mt-5">
    <div class="container mb-5">
        <div class="row">
            <div class="col-lg-6 p-5">
                <p style="font-size: 40px; color:#3b3b3b">Seja Bem Vindo Dev!</p>
                <hr id="hrB">
                <p style="font-size: 20px; color: #636363;">Um mundo de possibilidades!</p>
                <p style="text-align: justify; color: #313131
                ">Fala Dev, Esperamos que gostem do conteúdo e se interessem mais pela área de informática, além de
                adquirir conhecimentos específicos com os cursos que iremos liberar, você também poderá ter acesso a qualquer tipo de
                assunto voltado para a informática, é só nos contatar. Fique ligado no nosso instagram <a style="color: #313131" title="Nosso instagram" href="https://www.instagram.com/world_dev.proj/" target="_blank">@world_dev.proj</a> para não
                perder nenhuma informação e confira nosso blog, para outros conteúdos tecnológicos da atualidade!
            </p>
            </div>
            <div class="col p-lg-5 mt-lg-5">
                <img class="img-fluid" src="visual/imagens/lo.png" width="600">
            </div>
        </div>
    </div>
    <hr>

    <div class="bg-dark mt-5 mb-5" style="color: #fff; padding: 100px 0px;">
        <div class="container">
                <div class="text-center">
                    <p style="font-size: 70px;">Cursos</p>
                    <div class="row">
                        <div class="col-lg-4 col-md-4">
                        <div class="fundoI p-3 center">
                            <img src="visual/imagens/html.png" width="60">
                        </div>
                            <p style="font-size: 40px;">Html/Css</p>
                            <p class="text-center">Nessa disciplina você irá desfrutar do essencial para se iniciar seu
                            desenvolvimento em sites modernos para web. Podendo tirar dúvidas de forma direta para a equipe
                            e praticando nas aulas e resoluções de exercícios.
                        </p>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <p style="font-size: 100px;">X</p>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="fundoI p-2 center">
                                <img src="visual/imagens/lp.png" width="74">
                            </div>
                            <p style="font-size: 40px;">LP</p>
                            <p class="text-center">Em lógica de programação, você terá acesso exclusivo a todas as aulas,
                            resoluções de exercícios e muito mais! Poderá tirar dúvidas a vontade com qualquer orientador
                            responsável pela dísciplina.
                            </p>
                        </div>
                    </div>
                </div>
        </div>
    </div>

    <footer id="footer" class="p-lg-5 p-md-3 p-sm-3 px-md-4 bg-white">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <h5>Esperamos por você</h5>
                    Agradecemos sua presença, não deixe de fazer parte dessa experiência única. Com tudo organizado para você
                    apenas fazer bom uso, sucesso!
                </div>
                <div class="offset-lg-3 col">
                    <h5><a href="visual/sobrenos.html">Quer saber mais?</a></h5>
                    <p>Quem somos?</p>
                    <p>Qual nosso intuito?</p>

                </div>
                <div class="col">
                    <h5><a href="#a">Entre em contato</a></h5>
                    <p>
                    <img src="visual/imagens/insta.png" width="24">
                    <a style="color: #212529;" href="https://www.instagram.com/world_dev.proj/" target="_blank">@world_dev.proj</a></p>
                    <p><img src="visual/imagens/mail.png">
                    <a style="color: #212529;" href="https://mail.google.com/mail/u/0/" target="_blank">worlddevproj@gmail.com</a></p>
                </div>
            </div>
        </div>
    </footer>
    <script src="visual/js/jquery-3.4.1.min.js"></script>
    <script src="visual/js/sweetAlert.js"></script>
    <?php    
    if(isset($_SESSION["erroDel"])){
        if(!$_SESSION["erroDel"]){
            echo "<script>swal('Parabéns','Você deletou sua conta','success');</script>";
        }
        session_destroy();
    }    
    ?>
</body>
</html>
