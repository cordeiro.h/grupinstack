<?php
session_start();
echo"
<!DOCTYPE html>
<html>
<head>
    <title>Plataforma | Pré-inscrição</title>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
    <link rel='stylesheet' href='visual/css/bootstrap.css'>
    <link rel='stylesheet' href='visual/css/style.css'>
</head>
<body class='body'>
<div class='container'>
<center>
  <div class='row featurette'>
    <div class='col-md-5' id='ola'>
    	<center class='position'>
        <br />
        <img src='visual/imagens/logo.png' class='img'></img>
        <br />
        <h4 class='mb-0'>Um mundo de possibilidades</h4>
        <br />
        <p class='mb-0'>Venha conhecer o mundo do desenvolvimento e se inscreva! Depois se cadastre e aproveite.</p>
        <br />
        <a class='btn btn-outline-secondary disabled' href='cadastro.php' tabindex='-1' aria-disabled='true'>Cadastro</a>
      </center>
      <br>
    </div>
    <div class='' id='posicao' style='background-color: white;'>
    <div>
    <center>              
    </div>
    <div class='col-md-8' style='background-color: white;'>
      <div>
      <br>
        <center>
          <h4 class='font-italic'>Cadastro</h4>
          <br>
          <div class='col-md-9'>                        
            <form class='form vertical-alignC' action='controle/inscricao.php'  method='post'>                
              <div class='form-group'>                        
                <input type='text' id='nome' class='form-control' name='nome' placeholder='Como prefere ser chamado' required>
                <br>
                <input type='email' name='email' class='form-control' id='email' aria-describedby='emailHelp' placeholder='Seu email' required>
                <br>
                <input type='password' name='senha' id='senha' class='form-control' title='Mínimo 6 caracteres' aria-describedby='emailHelp' placeholder='Sua senha' minlength='6' maxlength='50' required>
                <br>
                <input type='password' name='confSenha' class='form-control' id='confSenha' aria-describedby='emailHelp' placeholder='Confirmar senha' minlength='6' maxlength='50' required>
                <br>
                 <label for='cars'>Escolha uma opção de aprendizado:</label>
                <select name='curso' class='form-control' id='cars'>
                  <option value='1'>Lógica de Programação</option>
                  <option value='2'>HTML CSS</option>
                  <option value='3'>Ambos os cursos</option>
                  <option value='0'>Apenas slids e acesso ao blog</option>
                </select>
                <br>
                <a id='termo' href='#'>Ler termo de compromisso.</a>
                <div class='form-check'>
                  <input class='form-check-input' type='checkbox' id='flexCheckDefault' required>
                  <label class='form-check-label' for='flexCheckDefault'>
                   Concordo com o termo de compromisso
                  </label>
                </div>
                <br>
                <center>
                  <input class='btn' tabindex='-1' style='color: white; background-color: #384b66;' value='Confirmar' type='submit'>
                </center>
                <br>
              </div>                
            </form> 
          </div> 
        </center>            
      </div>       
    </div>
  </div>
 </center>
 </div>
<script src='visual/js/jquery.js'></script>
<script src='visual/js/sweetAlert.js'></script>
<script src='visual/js/bootstrap.min.js' integrity='sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy' crossorigin='anonymous'></script>  
<script src='visual/js/inscricao.js'></script>
";
if(isset($_SESSION["erroSenha"])){
    echo"<script>swal('Atenção','Suas senhas são diferentes','warning');</script>";
    unset($_SESSION["erroSenha"]);
}
if(isset($_SESSION["erroMail"])){
	echo"<script>swal('Ops','Email não existe, por favor utilize um existente','error');</script>";
	unset($_SESSION["erroMail"]);
}
if(isset($_SESSION["erroInscricao"])){
    if($_SESSION["erroInscricao"]){
        echo"<script>swal('Ops','Desculpe, não foi possível inscrevê-lo tente novamente mais tarde','error');</script>";
    }else{
        echo"<script>swal('Inscrito','Agora você pertence ao World dev, bem vindo ao grupo!','success');</script>";
    }
    session_destroy();
}
echo"
</body>
</html>
";

?>