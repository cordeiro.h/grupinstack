$("#email").blur(function(){
    let email=$("#email").val();
    $.ajax({url:"controle/verificar.php?email="+email, success: function(resultado){
        if(resultado.length==9){
            swal("Atenção","Este email já foi inscrito","warning");
        }
    }});
});

$("#confSenha").blur(function(){
    if($("#senha").val()!=$("#confSenha").val()){
        swal("Atenção","Suas senhas são diferentes","warning");
    }
});
$("#senha").blur(function(){
    if($("#senha").val().length<6){
        swal("Atenção","Mínimo 6 caracteres para a senha","warning");
    }
});
$("#termo").click(function(){
    swal("Termo de compromisso",
    "Estou ciente de que todas as aulas serão utilizadas exclusivamente para estudo;\nQualquer palavra de baixo calão, discurso de ódio utilizados, poderá me prejudicar com advertências que podem levar a bloqueio na plataforma;\nA mudança de senhas estará por minha conta, nós não nos responsabilizamos pela perca da sua senha;\nEstou ciente de que todas as aulas serão utilizadas exclusivamente para estudo.", "info");
});
