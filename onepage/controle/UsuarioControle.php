<?php
require_once("Conect.php");
require_once("../modelo/Usuario.php");
class UsuarioControle{
    public function inserir($usuario){
        $retorno=false;
        try{
            $con=new Conexao();
            if($usuario->getCurso()==0){
                $cmd= $con->getConexao()->prepare("INSERT INTO usuario(nome,email,senha,curso,adm) VALUES(:n,:e,:s,0,0);");
            }else{
                $cmd=$con->getConexao()->prepare("INSERT INTO usuario(nome,email,senha,curso,adm) VALUES(:n,:e,:s,:c,0);");
                $curso=$usuario->getCurso();
            }
            $nome=$usuario->getNome();
            $email=$usuario->getEmail();
            $senha=$usuario->getSenha();
            $cmd->bindParam(":n",$nome);
            $cmd->bindParam(":e",$email);
            $cmd->bindParam(":s",$senha);
            if(isset($curso)){
                $cmd->bindParam(":c",$curso);
            }
            if($cmd->execute()){
                $retorno=true;
            }
            $con->fecharConexao();
            return $retorno;
        }catch(Exception $e){
            echo"Erro na inserção do usuario: {$e->getMessage()}";
            return $retorno;
        }
    }
    public function verificar($email){
        $retorno=false;
        try{
            $con=new Conexao();
            $cmd= $con->getConexao()->prepare("SELECT * FROM usuario WHERE email=:e;");
            $cmd->bindParam(":e",$email);
            if($cmd->execute()){
                $resultado=$cmd->fetchAll();
                if($resultado!=null){
                    $retorno=true;
                }
            }
            $con->fecharConexao();
            return $retorno;
        }catch(Exception $e){
            echo"Erro ao verificar email: {$e->getMessage()}";
            return $retorno;
        }
    }
}
?>
