# GruPinStack
### Objetivo
Expressar nosso conhecimento que adquirimos no tempo de curso, colocando em prática na forma de uma plataforma web responsiva com gestão de banco de dados. Adicionar aulas na plataforma referentes ao conteúdo de *Lógica de Programação* e *HTML/CSS*. Ademais, as aulas serão gravadas pelos responsáveis pelo projeto, disponibilizadas para os cadastrados na plataforma GRUPINSTACK a partir de Fevereiro de 2021.

---
#### Responsáveis
1. _**Emanuel Soares dos Santos**_
2. _**Kailane Ferreira Fonteles**_
3. _**Kauan Cordeiro de L Mesquita**_
4. _**Keyliane Gadelha Carvalho**_
5. _**Lara Giovana Sousa Mulato**_
6. _**Maria Isabel Martins de Souza.**_

---
