<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Crop and upload</title>
    <link rel="stylesheet" href="node_modules/croppie/croppie.css">
    <link rel="stylesheet" href="exemplo.css">
  <script src="jquery.min.js" type="text/javascript"></script>
  <script src="croppie.js" type="text/javascript"></script>
    <script src="node_modules/exif-js/exif.js" type="text/javascript"></script>
  <script src="exemplo.js" type="text/javascript"></script>
</head>
<body>
    <div id="response"></div>
    <div class="demo-wrap upload-demo">
        <div class="upload-demo-wrap">
            <div class="croppie-container" id="upload-demo"></div>
        </div>
    </div>
    <form method="post" id="img-upload" enctype="multipart/form-data">
        <label>Nome da imagem:(opcional)</label>
        <input type="text" name="name-img" class="form-vertical">
        <label>Imagem para recortar:</label>
        <input type="file" accept="image/*" id="img" class="form-vertical">
        <input type="submit" class="form-vertical">
    </form>
    <img id="img-preview" src="" alt="preview"> 
</body>
</html>